# Kanban - Station Project Header
```
* FILE:             Documentation\projectStation.md
* PROJECT:          Kanban 
* PROGRAMMERS:      PETTERSON AMARAL and PAULO CASADO (AKA PC)
* FIRST VERSION:    2021-Mar-23
* CURRENT VERSION:  2021-Mar-24
* DESCRIPTION:      This file provides about the Station.
```
[start_over](../README.md)
# Station description 
The Station or worker project (DB producer) has an automatic connection to the database, and it has as outcome create and item (fog lamp) based on a Kanban production line. All data is stored in this solution database and consumed by the other projects.

## Built With
* [C Sharp](https://docs.microsoft.com/en-us/dotnet/csharp/) - C# language
* [Windows Forms](https://docs.microsoft.com/en-us/dotnet/desktop/winforms/?view=netframeworkdesktop-4.8) - Graphical user interface (GUI) 
* DAL (Data Access Layer)



## Screenshots
Station starter form.

![StationStarter0](./imgs/StationStarter0.jpg)


Station starter form and its options.

![StationStarter1](./imgs/StationStarter1.jpg)


Station worker running and assembling fog lamps.

![StationStarter2](./imgs/StationStarter2.jpg)


