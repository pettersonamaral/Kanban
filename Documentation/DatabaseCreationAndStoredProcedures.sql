-- //********************************************************************************************************************************* 
-- // FILE          : 20180422_DatabaseReview_rev6_withSTP.sql
-- // PROJECT       : Final Project: DB CreationScript 
-- // COURSE	    : PROG2030 - Advanced SQL
-- // PROGRAMMER    : Paulo Casado (AKA PC)  & Petterson Amaral
-- // VERSION DATE  : 2018-APR-23
-- // DESCRIPTION   : The file store the script to create the databese "SQL_FinalProject_Kanban"
-- //**********************************************************************************************************************************

USE [master]
DECLARE @kill varchar(8000) = '';  
SELECT @kill = @kill + 'kill ' + CONVERT(varchar(5), session_id) + ';'  
FROM sys.dm_exec_sessions
WHERE database_id  = db_id('SQL_FinalProject_Kanban')

EXEC(@kill);
GO

DECLARE @DBName varchar(50) = 'SQL_FinalProject_Kanban'
USE master
IF EXISTS(select * from sys.databases where name= @DBName)
EXEC('DROP DATABASE ' + @DBName)
GO

CREATE DATABASE [SQL_FinalProject_Kanban]
GO

USE [SQL_FinalProject_Kanban]
GO

/****** Object:  Table [dbo].[config]    Script Date: 2018-04-23 3:18:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[config](
	[id] [int] NOT NULL,
	[config_name] [nvarchar](50) NOT NULL,
	[value] [nvarchar](50) NOT NULL,
	[data_type] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_config] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBin]    Script Date: 2018-04-23 3:18:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBin](
	[BinID] [int] NOT NULL,
	[PartID] [int] NOT NULL,
 CONSTRAINT [PK_tblBin_1] PRIMARY KEY CLUSTERED 
(
	[BinID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBinLine]    Script Date: 2018-04-23 3:18:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBinLine](
	[StationID] [int] NOT NULL,
	[BinID] [int] NOT NULL,
	[StockFlag] [nvarchar](3) NOT NULL,
	[CurrentQty] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblFogLamp]    Script Date: 2018-04-23 3:18:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblFogLamp](
	[FogLampID] [nvarchar](10) NOT NULL,
	[TestTrayID] [nvarchar](8) NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[TestPass] [nvarchar](3) NOT NULL,
 CONSTRAINT [PK_tblFogLamp] PRIMARY KEY CLUSTERED 
(
	[FogLampID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPart]    Script Date: 2018-04-23 3:18:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPart](
	[PartID] [int] NOT NULL,
	[PartName] [nvarchar](20) NOT NULL,
	[MaxBinQty] [int] NOT NULL,
	[MinBinQty] [int] NOT NULL,
 CONSTRAINT [PK_tblPart] PRIMARY KEY CLUSTERED 
(
	[PartID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblProduction]    Script Date: 2018-04-23 3:18:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProduction](
	[ProductionID] [int] NOT NULL,
	[StationID] [int] NOT NULL,
	[FogLampTotalQty] [int] NOT NULL,
	[FogLampPassQty] [int] NOT NULL,
	[FogLampDefectQty] [int] NOT NULL,
 CONSTRAINT [PK_tblProduction] PRIMARY KEY CLUSTERED 
(
	[ProductionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblStation]    Script Date: 2018-04-23 3:18:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblStation](
	[StationID] [int] NOT NULL,
	[WorkerID] [int] NOT NULL,
 CONSTRAINT [PK_tblStation_1] PRIMARY KEY CLUSTERED 
(
	[StationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTestTray]    Script Date: 2018-04-23 3:18:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTestTray](
	[TestTrayID] [nvarchar](8) NOT NULL,
	[FogLampQty] [int] NOT NULL,
 CONSTRAINT [PK_tblTestTray] PRIMARY KEY CLUSTERED 
(
	[TestTrayID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTestTrayLine]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTestTrayLine](
	[TestTrayID] [nvarchar](8) NOT NULL,
	[StationID] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblWorker]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWorker](
	[WorkerID] [int] NOT NULL,
	[WorkerExperience] [nvarchar](20) NOT NULL,
	[AvgAssemblyTime] [float] NOT NULL,
	[AvgDefectOcurrence] [float] NOT NULL,
 CONSTRAINT [PK_tblWorker] PRIMARY KEY CLUSTERED 
(
	[WorkerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[config] ([id], [config_name], [value], [data_type]) VALUES (1, N'runner_time', N'2', N'int')
INSERT [dbo].[config] ([id], [config_name], [value], [data_type]) VALUES (2, N'tray_capacity', N'60', N'int')
INSERT [dbo].[config] ([id], [config_name], [value], [data_type]) VALUES (3, N'avg_assembly', N'60', N'int')
INSERT [dbo].[config] ([id], [config_name], [value], [data_type]) VALUES (4, N'speed_time', N'50', N'float')
INSERT [dbo].[tblBin] ([BinID], [PartID]) VALUES (1, 1)
INSERT [dbo].[tblBin] ([BinID], [PartID]) VALUES (2, 2)
INSERT [dbo].[tblBin] ([BinID], [PartID]) VALUES (3, 3)
INSERT [dbo].[tblBin] ([BinID], [PartID]) VALUES (4, 4)
INSERT [dbo].[tblBin] ([BinID], [PartID]) VALUES (5, 5)
INSERT [dbo].[tblBin] ([BinID], [PartID]) VALUES (6, 6)
INSERT [dbo].[tblPart] ([PartID], [PartName], [MaxBinQty], [MinBinQty]) VALUES (1, N'Harness', 55, 5)
INSERT [dbo].[tblPart] ([PartID], [PartName], [MaxBinQty], [MinBinQty]) VALUES (2, N'Reflector', 35, 5)
INSERT [dbo].[tblPart] ([PartID], [PartName], [MaxBinQty], [MinBinQty]) VALUES (3, N'Housing', 24, 5)
INSERT [dbo].[tblPart] ([PartID], [PartName], [MaxBinQty], [MinBinQty]) VALUES (4, N'Lens', 40, 5)
INSERT [dbo].[tblPart] ([PartID], [PartName], [MaxBinQty], [MinBinQty]) VALUES (5, N'Bulb', 60, 5)
INSERT [dbo].[tblPart] ([PartID], [PartName], [MaxBinQty], [MinBinQty]) VALUES (6, N'Bezel', 75, 5)
INSERT [dbo].[tblWorker] ([WorkerID], [WorkerExperience], [AvgAssemblyTime], [AvgDefectOcurrence]) VALUES (1, N'Rookie', 50, 85)
INSERT [dbo].[tblWorker] ([WorkerID], [WorkerExperience], [AvgAssemblyTime], [AvgDefectOcurrence]) VALUES (2, N'Normal', 100, 50)
INSERT [dbo].[tblWorker] ([WorkerID], [WorkerExperience], [AvgAssemblyTime], [AvgDefectOcurrence]) VALUES (3, N'Super', 115, 15)
ALTER TABLE [dbo].[tblBin]  WITH CHECK ADD  CONSTRAINT [FK_tblBin_tblPart] FOREIGN KEY([PartID])
REFERENCES [dbo].[tblPart] ([PartID])
GO
ALTER TABLE [dbo].[tblBin] CHECK CONSTRAINT [FK_tblBin_tblPart]
GO
ALTER TABLE [dbo].[tblBinLine]  WITH CHECK ADD  CONSTRAINT [FK_tblBinLine_tblBin1] FOREIGN KEY([BinID])
REFERENCES [dbo].[tblBin] ([BinID])
GO
ALTER TABLE [dbo].[tblBinLine] CHECK CONSTRAINT [FK_tblBinLine_tblBin1]
GO
ALTER TABLE [dbo].[tblBinLine]  WITH CHECK ADD  CONSTRAINT [FK_tblBinLine_tblStation] FOREIGN KEY([StationID])
REFERENCES [dbo].[tblStation] ([StationID])
GO
ALTER TABLE [dbo].[tblBinLine] CHECK CONSTRAINT [FK_tblBinLine_tblStation]
GO
ALTER TABLE [dbo].[tblFogLamp]  WITH CHECK ADD  CONSTRAINT [FK_tblFogLamp_tblTestTray] FOREIGN KEY([TestTrayID])
REFERENCES [dbo].[tblTestTray] ([TestTrayID])
GO
ALTER TABLE [dbo].[tblFogLamp] CHECK CONSTRAINT [FK_tblFogLamp_tblTestTray]
GO
ALTER TABLE [dbo].[tblProduction]  WITH CHECK ADD  CONSTRAINT [FK_tblProduction_tblStation] FOREIGN KEY([StationID])
REFERENCES [dbo].[tblStation] ([StationID])
GO
ALTER TABLE [dbo].[tblProduction] CHECK CONSTRAINT [FK_tblProduction_tblStation]
GO
ALTER TABLE [dbo].[tblStation]  WITH CHECK ADD  CONSTRAINT [FK_tblStation_tblWorker] FOREIGN KEY([WorkerID])
REFERENCES [dbo].[tblWorker] ([WorkerID])
GO
ALTER TABLE [dbo].[tblStation] CHECK CONSTRAINT [FK_tblStation_tblWorker]
GO
ALTER TABLE [dbo].[tblTestTrayLine]  WITH NOCHECK ADD  CONSTRAINT [FK_tblTestTrayLine_tblStation] FOREIGN KEY([StationID])
REFERENCES [dbo].[tblStation] ([StationID])
GO
ALTER TABLE [dbo].[tblTestTrayLine] NOCHECK CONSTRAINT [FK_tblTestTrayLine_tblStation]
GO
ALTER TABLE [dbo].[tblTestTrayLine]  WITH CHECK ADD  CONSTRAINT [FK_tblTestTrayLine_tblTestTray] FOREIGN KEY([TestTrayID])
REFERENCES [dbo].[tblTestTray] ([TestTrayID])
GO
ALTER TABLE [dbo].[tblTestTrayLine] CHECK CONSTRAINT [FK_tblTestTrayLine_tblTestTray]
GO
/****** Object:  StoredProcedure [dbo].[addBinInfo]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[addBinInfo] (@binID AS INT, @partID AS INT)
AS
BEGIN

	-- Insert data into the Bin table
	INSERT INTO tblBin (BinID, PartID)
	VALUES (@binID, @partID)

	-- Check if insert was sucessful
	IF @@ROWCOUNT = 1
		RETURN 1
	ELSE
		RETURN 0
END
-------------------------------------------------------------------------------------------------------------------------------------------

-- [END] STATION BIN CONFIGURATION ========================================================================================================


-- [START] STATION TEST TRAY CONFIGURATION ================================================================================================

-- ADD STATION TEST TRAY INFO TO tblTestTray ----------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[addBinLineInfo]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[addBinLineInfo] (@stationID AS INT, @binID AS INT, @stockFlag AS NVARCHAR(3),@currentQty AS INT)
AS
BEGIN

	-- Insert data into the BinLine table
	INSERT INTO tblBinLine (StationID, BinID, StockFlag, CurrentQty)
	VALUES (@stationID, @binID, @stockFlag, @currentQty)

	-- Check if insert was sucessful
	IF @@ROWCOUNT = 1
		RETURN 1
	ELSE
		RETURN 0
END
------------------------------------------------------------------------------------------------------------------------------------------


-- CREATE STORAGE PROCEDURE TO FILL TTHE TABLE tblBin FOR A SPECIFIC STATION -------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[addLampCounttoTestTray]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[addLampCounttoTestTray] (@testTrayID AS NVARCHAR(8), @lampCount AS INT)
AS
BEGIN

	--  Increment lamp count
	UPDATE tblTestTray
	SET FogLampQty = @lampCount
	WHERE TestTrayID = @testTrayID

RETURN 1
END


-- [END] STATION PRODUCTION INTERVAL CHECKERS =================================================================================================





GO
/****** Object:  StoredProcedure [dbo].[addNewFogLampID]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[addNewFogLampID] (@newFogLampID AS NVARCHAR(10), @testTrayID AS NVARCHAR(8), @timeStamp AS DATETIME, @passTest AS NVARCHAR(3))
AS
BEGIN

	-- Insert new lamp into into the tblFogLamp
	INSERT INTO tblFogLamp (FogLampID, TestTrayID, TimeStamp, TestPass)
	VALUES (@newFogLampID, @testTrayID, @timeStamp, @passTest)

	-- Check if insert was sucessful
	IF @@ROWCOUNT = 1
		RETURN 1
	ELSE
		RETURN 0
END
-----------------------------------------------------------------------------------------------------------------------------------------------

-- CREATE A STORAGE PROCEDURE TO DECREASE THE PART UNITS FROM THE BIN--------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[addtblStationInfo]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[addtblStationInfo] (@newStationID AS INT, @selectedWorkerID AS INT)
AS
BEGIN

	-- Check if current StationID already exists, and return accordingly
	DECLARE @existing AS int = 0
	SELECT  @existing = count(StationID)
	FROM    tblStation
    WHERE StationID = @newStationID
	
	-- If existing, return an error (0 in this case)
	IF @existing > 0 
	BEGIN
		RAISERROR('StationID already exists', 1, 1)
		RETURN 0
	END

	-- Insert Station Data
	INSERT INTO tblStation (StationID, WorkerID)
	VALUES (@newStationID, @selectedWorkerID)

	-- Check if insert was sucessful
	IF @@ROWCOUNT = 1
		RETURN 1
	ELSE
		RETURN 0
END
-----------------------------------------------------------------------------------------------------------------------------------------

-- [END] STATION SETUP ==================================================================================================================


-- [START] STATION BIN CONFIGURATION ====================================================================================================

-- Create Storage Procedure to retrive Bin Configuration from Config Table --------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[addTestTrayInfo]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[addTestTrayInfo] (@newTestTrayID AS NVARCHAR(8), @stationID AS INT)
AS
BEGIN
	
	DECLARE @initialQty AS INT = 0

	-- Insert TestTrayID into tblTestTray
	INSERT INTO tblTestTray (TestTrayID, FogLampQty)
	VALUES (@newTestTrayID, @initialQty)

	-- Check if insert was sucessful
	IF @@ROWCOUNT = 1
		RETURN 1
	ELSE
		RETURN 0
END
--------------------------------------------------------------------------------------------------------------------------------------------

-- ADD STATION TEST TRAY INFO TO tblTestTrayLine--------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[addTestTrayLineInfo]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[addTestTrayLineInfo] (@newTestTrayID AS NVARCHAR(8), @stationID AS INT)
AS
BEGIN
	-- Insert Test Tray Data into tblTestTray
	INSERT INTO tblTestTrayLine (TestTrayID, StationID)
	VALUES (@newTestTrayID, @stationID)

	-- Check if insert was sucessful
	IF @@ROWCOUNT = 1
		RETURN 1
	ELSE
		RETURN 0
END
--------------------------------------------------------------------------------------------------------------------------------------------

-- CREATE A STORAGE PROCEDURE TO RETRIEVE THE LAST TestTrayID VALUE FROM tblTestTray -------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[changeBinStockFlag]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[changeBinStockFlag] (@flagValue AS NVARCHAR(3), @stationID AS INT, @binPartID AS INT)
AS
BEGIN

	--  new lamp into into the tblFogLamp
	UPDATE tblBinLine 
	SET StockFlag = @flagValue
	WHERE StationID = @stationID AND BinID = @binPartID

RETURN 1
END
------------------------------------------------------------------------------------------------------------------------------------------------

-- CREATE STORAGE PROCEDURE TO GET CURRENT BIN PARTS COUNT IN THE DATABASE----------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[checkBinPartsCount]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[checkBinPartsCount] (@stationID AS int, @binPartID AS INT)
AS
BEGIN

	--  retrieve the current count
	SELECT CurrentQty 
	FROM tblBinLine
	WHERE StationID = @stationID AND BinID = @binPartID

RETURN 1
END
-----------------------------------------------------------------------------------------------------------------------------------------------

-- CREATE STORAGE PROCEDURE TO INCREMENT LAMP COUNT ON tblTestTray
GO
/****** Object:  StoredProcedure [dbo].[checkLastStationID]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[checkLastStationID]
AS
BEGIN
	SELECT StationID
	FROM tblStation
	RETURN 1
END
---------------------------------------------------------------------------------------------------------------------------------------

-- CREATE STORAGE PROCEDURE TO ADD INFORMATION INTO tbStation -------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[getAllStations]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getAllStations]
AS
BEGIN

	SELECT StationID, WorkerExperience 
	FROM tblStation
	JOIN tblWorker
	ON tblStation.WorkerID = tblWorker.WorkerID
	
	RETURN 1
END


-- [END] ANDON SETUP ========================================================================================================

-- [START] STATION BIN CONFIGURATION ========================================================================================================
GO
/****** Object:  StoredProcedure [dbo].[getAllWorkerCategories]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getAllWorkerCategories]
AS
BEGIN
	SELECT WorkerExperience from tblWorker
	RETURN 1
END
--------------------------------------------------------------------------------------------------------------------------------------

-- CREATE STORAGE PROCEDURE TO RETRIEVE TIME SCALE FROM tblConfig
GO
/****** Object:  StoredProcedure [dbo].[getInitialBinPartsConfig]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getInitialBinPartsConfig]
AS
BEGIN
	SELECT PartID, PartName, MaxBinQty, MinBinQty
	FROM tblPart
	ORDER BY PartID
RETURN 1
END
-----------------------------------------------------------------------------------------------------------------------------------------

-- CREATE STORAGE PROCEDURE TO FILL TTHE TABLE tblBinLine FOR A SPECIFIC STATION --------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[getLastTestTrayID]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getLastTestTrayID]
AS
BEGIN

	SELECT TOP 1 TestTrayID
	FROM tblTestTray
	ORDER BY TestTrayID DESC

RETURN 1
END
---------------------------------------------------------------------------------------------------------------------------------------------


-- CREATE A STORAGE PROCEDURE TO COUNT THE NUMBER OF LAMPS INSIDE A TEST TRAY ---------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[getNumOfLampsOnTray]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getNumOfLampsOnTray](@testTrayID AS NVARCHAR(8))
AS
BEGIN

	SELECT COUNT(FogLampQty)
	FROM tblTestTray
	WHERE TestTrayID = @testTrayID

RETURN 1
END
----------------------------------------------------------------------------------------------------------------------------------------------

-- [END] STATION TEST TRAY CONFIGURATION =====================================================================================================

-- [START] STATION PRODUCTION INTERVAL CHECKERS ==============================================================================================

-- CREATE STORAGE PROCEDURE TO ADD A NEW LAMP TO THE TEST TRAY -------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[getTimeFactor]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getTimeFactor]
AS
BEGIN
	SELECT [value] 
	FROM config
	WHERE config_name = 'speed_time'
	RETURN 1
END


-- [END] STATION STARTER SETUP ========================================================================================================


-- [START] STATION SETUP ==============================================================================================================

--  GET THE LATEST STATIONID VALUE FROM THE DATABASE ----------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[loadNeededBins]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--ALTER PROCEDURE loadNeededBins
CREATE PROCEDURE [dbo].[loadNeededBins]
	@parBinID INT,
	@parStationID INT,
	@parPartID INT,
	@parCurrentQty INT
AS
BEGIN
	DECLARE	
	@CurrentQty INT,
	@maxProductQty INT

		SELECT @maxProductQty = p.MaxBinQty, @CurrentQty = bl.CurrentQty
			FROM tblBinLine bl
			INNER JOIN tblBin b ON bl.BinID = b.BinID
			INNER JOIN tblPart p ON p.PartID = b.PartID
			AND b.BinID = @parBinID
			AND bl.StationID = @parStationID

	BEGIN TRAN

		UPDATE tblBinLine
			SET CurrentQty = @CurrentQty + @maxProductQty,
			StockFlag  = '0'
			WHERE BinID = @parBinID 
			AND StationID = @parStationID

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
		END
	COMMIT TRAN
END
GO
/****** Object:  StoredProcedure [dbo].[runner_running]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--ALTER PROCEDURE [dbo].[runner_running]
CREATE PROCEDURE [dbo].[runner_running]
AS
BEGIN
	DECLARE	
	@BinID INT,
	@StationID INT,
	@PartID INT,
	@CurrentQty INT


	-- Creates Cursos to traverse the tables that have flagged true.
	DECLARE getNeededBins CURSOR FOR   
		SELECT bl.BinID, bl.StationID, p.PartID, bl.CurrentQty
		  FROM tblBinLine bl
		  INNER JOIN tblBin b ON bl.BinID = b.BinID
		  INNER JOIN tblPart p ON p.PartID = b.PartID
		  WHERE bl.StockFlag = '1'

	OPEN getNeededBins

	FETCH NEXT FROM getNeededBins INTO @BinID, @StationID, @PartId, @CurrentQty

	WHILE (@@FETCH_STATUS=0)
	BEGIN
		-- Runner running to load needed bins
		EXECUTE loadNeededBins @BinID, @StationID, @PartId, @CurrentQty

		FETCH NEXT FROM getNeededBins INTO @BinID, @StationID, @PartId, @CurrentQty
	END

	CLOSE getNeededBins
	DEALLOCATE getNeededBins
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PassFail]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_PassFail]
AS
BEGIN
	DECLARE	
	@paramDateIni DATE = getdate(),
	@paramDateEnd DATE = getdate() + 1

	SELECT fl.TestPass, Count(fl.TestPass) AS CountPassFail
	FROM ((tblTestTrayLine ttl INNER JOIN tblStation s ON ttl.StationID = s.StationID) INNER JOIN (tblTestTray tt 
	INNER JOIN tblFogLamp fl ON tt.TestTrayID = fl.TestTrayID) ON ttl.TestTrayID = tt.TestTrayID) INNER JOIN tblWorker w 
	ON s.WorkerID = w.WorkerID
	AND fl.TimeStamp >= @paramDateIni
	AND fl.TimeStamp < @paramDateEnd
	GROUP BY fl.TestPass;

END

GO
/****** Object:  StoredProcedure [dbo].[sp_ProductionByProfile]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_ProductionByProfile]
AS
BEGIN
	DECLARE	
	@paramDateIni DATE = getdate(),
	@paramDateEnd DATE = getdate() + 1

	SELECT w.WorkerExperience, Count(tt.FogLampQty) AS TotProfile
	FROM ((tblTestTrayLine ttl INNER JOIN tblStation s ON ttl.StationID = s.StationID) 
	INNER JOIN (tblTestTray tt 
	INNER JOIN tblFogLamp fl ON tt.TestTrayID = fl.TestTrayID) 
	ON ttl.TestTrayID = tt.TestTrayID) 
	INNER JOIN tblWorker w ON s.WorkerID = w.WorkerID
	WHERE fl.TimeStamp >= @paramDateIni
	AND fl.TimeStamp < @paramDateEnd
	GROUP BY w.WorkerExperience

END



GO
/****** Object:  StoredProcedure [dbo].[sp_ProductionBySation]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_ProductionBySation]
AS
BEGIN
	DECLARE	
	@paramDateIni DATE = getdate(),
	@paramDateEnd DATE = getdate() + 1

	SELECT ttl.StationID, COUNT(tt.FogLampQty) AS Totalqty
	FROM tblTestTray tt, tblTestTrayLine ttl, tblFogLamp tfl
	WHERE tt.TestTrayID = ttl.TestTrayID 
	AND tt.TestTrayID = tfl.TestTrayID
	AND tfl.TimeStamp >= @paramDateIni
	AND tfl.TimeStamp < @paramDateEnd
	GROUP BY ttl.StationID


END



GO
/****** Object:  StoredProcedure [dbo].[subtractBinPart]    Script Date: 2018-04-23 3:18:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[subtractBinPart] (@stationID AS INT, @binID AS INT, @decreaseNumber AS INT)
AS
BEGIN

	-- Update number of parts from a bin current qty
	UPDATE tblBinLine 
	SET CurrentQty = CurrentQty - @decreaseNumber
	WHERE StationID = @stationID AND BinID = @binID

RETURN 1
END
-----------------------------------------------------------------------------------------------------------------------------------------------

-- CREATE A STORAGE PROCEDURE TO CHANGE BIN STOCK FLAG ----------------------------------------------------------------------------------------
GO
USE [master]
GO
ALTER DATABASE [SQL_FinalProject_Kanban] SET  READ_WRITE 
GO
