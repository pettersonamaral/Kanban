# Kanban - ConfigurationTool Project Header
```
* FILE:             Documentation\projectConfigurationTool.md
* PROJECT:          Kanban 
* PROGRAMMERS:      PETTERSON AMARAL and PAULO CASADO (AKA PC)
* FIRST VERSION:    2021-Mar-23
* CURRENT VERSION:  2021-Mar-24
* DESCRIPTION:      This file provides about the ConfigurationTool.
```
[start_over](../README.md)
# ConfigurationTool description 
The configuration tool has a manual connection to the database and provides a Windows Forms GUI that allows users to set new values before starting the solution. 

## Common use 
The most common use is to set up the speed_time (50) and runner_time (2) to demo speeds.

1. runner_time -> It means how many times the runner will check for a recharge request every minute of program's time. 
2. speed_time -> It is a time scale where 1 means each minute in the program takes about 60 real seconds, and 50 means each minute in the program takes about 10 real seconds.    
 

## Built With
* [C Sharp](https://docs.microsoft.com/en-us/dotnet/csharp/) - C# language
* [Windows Forms](https://docs.microsoft.com/en-us/dotnet/desktop/winforms/?view=netframeworkdesktop-4.8) - Graphical user interface (GUI) 
* [ActiveX Data Objects](https://docs.microsoft.com/en-us/sql/ado/guide/data/ado-fundamentals?view=sql-server-ver15) - ADO



## Screenshots
ConfigurationTool disconnected. Welcome tab control.

![ConfigurationTool_disconnected](./imgs/ConfigurationToolDisconnected.jpg)


ConfigurationTool Connected. The General tab page.

![ConfigurationTool_Connected0](./imgs/ConfigurationToolConnected0.jpg)


ConfigurationTool Connected. The Products tab page.

![ConfigurationTool_Connected1](./imgs/ConfigurationToolConnected1.jpg)


ConfigurationTool Connected. The Profiles tab page. 

![ConfigurationTool_Connected2](./imgs/ConfigurationToolConnected2.jpg)


ConfigurationTool Connected displaying a message box.  

![ConfigurationTool_Connected3](./imgs/ConfigurationToolConnected3.jpg)
