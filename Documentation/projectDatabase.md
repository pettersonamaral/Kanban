# Kanban - Database doc Header
```
* FILE:             Documentation\Database_initialization.md
* PROJECT:          Kanban 
* PROGRAMMERS:      PETTERSON AMARAL and PAULO CASADO (AKA PC)
* FIRST VERSION:    2021-Mar-23
* CURRENT VERSION:  2021-Mar-23
* DESCRIPTION:      This file provides instructions who to setup the KANBAN database.
```
[start_over](../README.md)
# Database 
This project uses DAL (Data Access Layer), Transactions, and ADO ([ActiveX Data Objects](https://docs.microsoft.com/en-us/sql/ado/guide/data/ado-fundamentals?view=sql-server-ver15)) as database major technologies.  

# Database initialization 
Open Microsoft SQL Server Management (SSMS) tool, select and connect to the database host. Open the provided SQL [script file](./DatabaseCreationAndStoredProcedures.sql) and click “Execute” to run the script. This action should create the Kanban database and the related storage procedures.  
Be sure that all solution programs have the correct connection string and network rights to connect to the SQL Server database through the network.

## The script file 
The script file has all the logic to create all tables and storage procedures for this solution database.

1. Before running the solution, reset/initialize the database with initial data. see the [DatabaseCreationAndStoredProcedures.sql](./DatabaseCreationAndStoredProcedures.sql)
 

## Built With
* [SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-downloads) - SQL Server on-premises
* [SQL Server Docker](https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-docker?view=sql-server-ver15&pivots=cs1-bash) - SQL Server on-premises in a Docker container
* [SQL Server Docker Compose](https://forums.docker.com/t/ms-sql-server-with-docker-volumes/97407) - SQL Server on-premises in a Docker container with compose configuration file    


## Screenshots
ER diagram

![Runner_disconnected](./imgs/DBMS_ER_Diagram.jpg)

