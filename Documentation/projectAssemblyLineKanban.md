# Kanban - AssemblyLineKanban Project Header
```
* FILE:             Documentation\projectAssemblyLineKanban.md
* PROJECT:          Kanban 
* PROGRAMMERS:      PETTERSON AMARAL and PAULO CASADO (AKA PC)
* FIRST VERSION:    2021-Mar-23
* CURRENT VERSION:  2021-Mar-24
* DESCRIPTION:      This file provides about the AssemblyLineKanban.
```
[start_over](../README.md)
# AssemblyLineKanban description 
The AssemblyLineKanban project has a manual connection to the database and provides a Windows Forms GUI that allows users to see the daily production with an automatic refresh. 

## Built With
* [C Sharp](https://docs.microsoft.com/en-us/dotnet/csharp/) - C# language
* [Windows Forms](https://docs.microsoft.com/en-us/dotnet/desktop/winforms/?view=netframeworkdesktop-4.8) - Graphical user interface (GUI) 
* [ActiveX Data Objects](https://docs.microsoft.com/en-us/sql/ado/guide/data/ado-fundamentals?view=sql-server-ver15) - ADO



## Screenshots
AssemblyLineKanban disconnected.

![AssemblyLineKanban0](./imgs/AssemblyLineKanban0.jpg)


AssemblyLineKanban Connected.

![AssemblyLineKanban1](./imgs/AssemblyLineKanban1.jpg)


AssemblyLineKanban0 Connected and running.

![AssemblyLineKanban2](./imgs/AssemblyLineKanban2.jpg)


