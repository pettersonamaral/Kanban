# Kanban - Andon Project Header
```
* FILE:             Documentation\projectAndon.md
* PROJECT:          Kanban 
* PROGRAMMERS:      PETTERSON AMARAL and PAULO CASADO (AKA PC)
* FIRST VERSION:    2021-Mar-23
* CURRENT VERSION:  2021-Mar-24
* DESCRIPTION:      This file provides about the Andon.
```
[start_over](../README.md)
# Andon description 
The Andon project has an automatic connection to the database, and it has, as an outcome, retrieve snapshots on demand from a Kanban production line. There is no automatic actualization.

## Built With
* [C Sharp](https://docs.microsoft.com/en-us/dotnet/csharp/) - C# language
* [Windows Forms](https://docs.microsoft.com/en-us/dotnet/desktop/winforms/?view=netframeworkdesktop-4.8) - Graphical user interface (GUI) 
* DAL (Data Access Layer)



## Screenshots

Andon form just after a refresh.

![Andon0](./imgs/Andon0.jpg)

Andon form and its options.

![Andon1](./imgs/Andon1.jpg)



