# Kanban - Runner Project Header
```
* FILE:             Documentation\projectRunner.md
* PROJECT:          Kanban 
* PROGRAMMERS:      PETTERSON AMARAL and PAULO CASADO (AKA PC)
* FIRST VERSION:    2021-Mar-23
* CURRENT VERSION:  2021-Mar-24
* DESCRIPTION:      This file provides about the Runner.
```
[start_over](../README.md)
# Runner description 
This project fulfills worker's assembly stations when they are run out of parts.
The Runner project has a manual connection to the database and provides a Windows Forms GUI that allows users to controls its initialization. 

## Built With
* [C Sharp](https://docs.microsoft.com/en-us/dotnet/csharp/) - C# language
* [Windows Forms](https://docs.microsoft.com/en-us/dotnet/desktop/winforms/?view=netframeworkdesktop-4.8) - Graphical user interface (GUI) 
* [ActiveX Data Objects](https://docs.microsoft.com/en-us/sql/ado/guide/data/ado-fundamentals?view=sql-server-ver15) - ADO



## Screenshots
Runner disconnected.

![Runner_disconnected](./imgs/RunnerDisconnected.jpg)


Runner Connected.

![Runner_connected0](./imgs/RunnerConnected0.jpg)


Runner Connected and running.

![Runner_connected0](./imgs/RunnerConnected1.jpg)


