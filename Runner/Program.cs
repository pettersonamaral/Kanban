﻿/*****************************************************************************************************
* FILE : 		  Program.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-08


* DESCRIPTION/Requirements: 
1. To develop a SQL-based simulation solution
2. To create and use triggers, stored procedures, stored functions and views to model 
   business/production line workflow

   The customer builds a fog lamp assembly for automobiles. The assembly is made up of a harness, a 
   reflector, a housing, a lens, a bulb and a bezel. Each of these parts is in its bin, and an 
   employee takes one of each of these parts and puts it together. Once completed, the fog lamp is 
   placed on a tray with separators. When full, the tray is sent to the testing department (e.g. 
   these are the ‘test trays’ mentioned below).

* Refence/sources:

*****************************************************************************************************/
using System;
using System.Windows.Forms;

namespace Runner
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new RunnerMainForm());
        }
    }
}
