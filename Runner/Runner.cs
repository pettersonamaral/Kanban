﻿/*****************************************************************************************************
* FILE : 		  Runner.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-08


* DESCRIPTION/Requirements: 
    Creates the Runner Class as Singleton and its methods
  
    
* Refence/sources:
*****************************************************************************************************/
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using SQL_FinalProject;


namespace Runner
{
    public sealed class Runner
    {
        #region Public Constants 
        public const String LabelStarted = "Timer started.";
        public const String LabelStopped = "Timer stopped.";
        #endregion


        #region Attributes
        public Boolean IsRunnerStarted  { get; set; }                                           //Set / Get runner status (Started = True || Stopped = False)
        public Single TimeScale         { get; set; }                                           // Get time scale
        public Int32 RunnerTime         { get; set; }                                           // Get time scale
        #endregion


        #region Properties
        private static volatile Runner instance;                                                //Implementation Strategy - Multithreaded Singleton
        private static object syncRoot = new Object();                                          //Implementation Strategy - Multithreaded Singleton
        private Database myDatabase = Database.Instance;                                        //Load/Get Database Class instance
        static private SqlCommand cmd = new SqlCommand();
        #endregion


        #region Construtor
        /// <summary>
        /// Default constructor
        /// </summary>
        private Runner()
        {
            IsRunnerStarted = false;
        }
        #endregion


        #region Public Methods
        // --- Starts Singleton Pattern
        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        public static Runner Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new Runner();
                    }
                }
                return instance;
            }
        }
        // --- ends Singleton Pattern


        /// <summary>
        /// It starts the Runner
        /// </summary>
        /// <returns></returns>
        public bool Start()
        {
            bool setReturMethod = true;

            if (!GetConfigurationValues()) { setReturMethod = false; }                     //Load all Attributes values
            else { IsRunnerStarted = true; }                                               //If GetConfiguration doesn't fail returns true.

            return setReturMethod;
        }


        /// <summary>
        /// It stops the Runner
        /// </summary>
        /// <returns></returns>
        public bool Stop()
        {
            bool setReturMethod = true;


            IsRunnerStarted = false;
            return setReturMethod;
        }


        /// <summary>
        /// It stops the Runner
        /// </summary>
        /// <returns></returns>
        public bool Run()
        {
            bool setReturMethod = true;

            String query = Helper.GetStoredProcedureRunRunner();

            try
            {
                myDatabase.Conn.Open();                                   //Open connection


                using (SqlCommand mySqlCommand = new SqlCommand(query, myDatabase.Conn))
                {
                    mySqlCommand.CommandType = CommandType.StoredProcedure;
                    mySqlCommand.ExecuteNonQuery();
                }

                myDatabase.Conn.Close();                                  //Close connection
            }
            catch (Exception ex)
            {
                myDatabase.Conn.Close();
                IsRunnerStarted = false;
                setReturMethod = false;
                MessageBox.Show(ex.Message.ToString(), "Failure to run Runner!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }


            return setReturMethod;
        }


        #endregion


        #region Private Methods

        /// <summary>
        /// It stops the Runner
        /// </summary>
        /// <returns></returns>
        private bool GetConfigurationValues()
        {
            bool setReturMethod = true;

            if (!GetTimeScale())    { setReturMethod = false; }
            if (!GetRunnerTime())   { setReturMethod = false; }

            return setReturMethod;
        }


        /// <summary>
        /// Get timescale value from the database
        /// </summary>
        private bool GetTimeScale()
        {
            bool setReturMethod = true;

            String query = Helper.GetSelectTimeScale();
            SqlDataReader myDataReader = null;

            try
            {
                myDatabase.Conn.Open();                                   //Open connection
                cmd.Connection = myDatabase.Conn;                         //Load command.Connection Attribute with value from connection

                cmd.CommandText = query;                                  //Load command.CommandText Attribute with value from query

                myDataReader = cmd.ExecuteReader();

                while (myDataReader.Read())
                { 
                    TimeScale = Convert.ToSingle(myDataReader["value"]);
                }

                myDatabase.Conn.Close();                                  //Close connection
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                myDatabase.Conn.Close();
                setReturMethod = false;
                MessageBox.Show(ex.Message.ToString(), "Failure to load TimeScale value!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return setReturMethod;
        }


        /// <summary>
        /// Get runner_time value from the database
        /// </summary>
        private bool GetRunnerTime()
        {
            bool setReturMethod = true;

            String query = Helper.GetSelectRunnerTime();
            SqlDataReader myDataReader = null;

            try
            {
                myDatabase.Conn.Open();                                   //Open connection
                cmd.Connection = myDatabase.Conn;                         //Load command.Connection Attribute with value from connection

                cmd.CommandText = query;                                  //Load command.CommandText Attribute with value from query

                myDataReader = cmd.ExecuteReader();

                while (myDataReader.Read())
                {
                    RunnerTime = Convert.ToInt32(myDataReader["value"]);
                }

                myDatabase.Conn.Close();                                  //Close connection
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                myDatabase.Conn.Close();
                setReturMethod = false;
                MessageBox.Show(ex.Message.ToString(), "Failure to load Runner time value!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return setReturMethod;
        }
    }
    #endregion
}

