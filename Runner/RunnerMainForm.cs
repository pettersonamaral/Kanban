﻿/*****************************************************************************************************
* FILE : 		  RunnerMainForm.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr


* DESCRIPTION/Requirements: 
1. To develop a SQL-based simulation solution
2. To create and use triggers, stored procedures, stored functions and views to model 
   business/production line workflow

   The customer builds a fog lamp assembly for automobiles. The assembly is made up of a harness, a 
   reflector, a housing, a lens, a bulb and a bezel. Each of these parts is in its bin, and an 
   employee takes one of each of these parts and puts it together. Once completed, the fog lamp is 
   placed on a tray with separators. When full, the tray is sent to the testing department (e.g. 
   these are the ‘test trays’ mentioned below).

* Refence/sources:

*****************************************************************************************************/

using SQL_FinalProject;
using System;
using System.Windows.Forms;

namespace Runner
{
    public partial class RunnerMainForm : Form
    {
        private Database myDatabase = Database.Instance;                //Load/Get Database Class instance
        private Runner myRunner     = Runner.Instance;                  //Load/Get Runner Class instance
        private const Int32 ticksPerSeconds = 1000;                     //A timer multiplier
        private const Int32 workspeed = 12;                             //A timer multiplier


        public RunnerMainForm()
        {
            InitializeComponent();

            if (!myDatabase.ConnectionStatus)                           // If myDatabase is NOT connected
            {
                ItIsDisconnected();
            }
        }


        /// <summary>
        /// Button Connection. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSourceConnection_Click(object sender, EventArgs e)
        {
            try
            {
                if (myDatabase.ConnectionStatus)                            // If myDatabase is already connected
                {
                    if (myDatabase.Disconnect())                            // If set up a new connection is true
                    {
                        ItIsDisconnected();
                    }
                }
                else                                                        // If myDatabase is NOT connected
                {
                    if (myDatabase.Connect())                               // If set up a new connection is return true
                    {
                        ItIsConnected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Exception!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        /// <summary>
        /// Controls the Start button action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnStart_Click(object sender, EventArgs e)
        {
            StartRunner();
        }


        /// <summary>
        /// Controls the Stop button action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnStop_Click(object sender, EventArgs e)
        {
            StopRunner();
        }


        /// <summary>
        /// Tick timer function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RunnerTimerTick(object sender, EventArgs e)
        {
            if(myRunner.IsRunnerStarted)
            {
                myRunner.Run();
            }
            else
            {
                StopRunner();
            }
        }


        #region Support Methods

        /// <summary>
        /// Support method to Start Runner
        /// </summary>
        private void ToggleRunnerButtons()
        {
            if (myRunner.IsRunnerStarted)
            {
                btnStart.Enabled = false;
                btnStop.Enabled = true;
            }
            else
            {
                btnStart.Enabled = true;
                btnStop.Enabled = false;
            }
        }


        /// <summary>
        /// Support method to Connected status true
        /// </summary>
        private void ItIsConnected()
        {
            lblSourceConnectionStatus.Text = Database.CONNECTEDSTATUS;
            lblSourceDbNameStatus.Text = Database.DBNAME + myDatabase.DbName.ToString();
            picConnected.Visible = true;
            myDatabase.ConnectionStatus = true;
            btnStart.Enabled = true;
        }


        /// <summary>
        /// Support method to Connected status false
        /// </summary>
        private void ItIsDisconnected()
        {
            StopRunner();
            lblSourceConnectionStatus.Text = Database.DISCONNECTEDSTATUS;
            lblSourceDbNameStatus.Text = "";
            picConnected.Visible = false;
            myDatabase.ConnectionStatus = false;
            btnStart.Enabled = false;
            btnStop.Enabled = false;
        }


        /// <summary>
        /// Support method to Stop Runner
        /// </summary>
        private void StopRunner()
        {
            runnerTimer.Stop();

            if (myRunner.Stop())
            {               
                lblControlSatus.Text = Runner.LabelStopped;
                lblControlTimer.Text = "";
                ToggleRunnerButtons();
            }
        }


        /// <summary>
        /// Support method to Start Runner
        /// </summary>
        private void StartRunner()
        {
            if (myRunner.Start())
            {
                ToggleRunnerButtons();
                lblControlSatus.Text = Runner.LabelStarted;
                //ticksPerSeconds       => 1000, it means 1 second
                //myRunner.RunnerTime   => Defulf value 5 (it means 5 min), so we need to multiply by 60
                //myRunner.TimeScale    => A way to manage the interval. Defaul value is 1. If we want 
                //                      => to run the system fastest we should setup to 60 (it means one 
                //                      => second is equal a minute). We must use the configuration tool to do it.
                runnerTimer.Interval = (Int32)((ticksPerSeconds * myRunner.RunnerTime * 60) / myRunner.TimeScale);
                runnerTimer.Start();
            }
        }
        #endregion
    }
}
