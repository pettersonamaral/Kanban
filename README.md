# Kanban Project Header
```
* FILE:             README.md
* PROJECT:          Kanban 
* PROGRAMMERS:      PETTERSON AMARAL and PAULO CASADO (AKA PC)
* FIRST VERSION:    2018-Apr-22
* CURRENT VERSION:  2021-Mar-24
* DESCRIPTION:      This file provides instructions to run the Fog Lamp KANBAN simulation.
```

# Kanban Project, 20,000 feet view
On the Advanced SQL course of the Conestoga Software Engineering Technician program, PC and I have built this solution. The requirement was to develop a simulation where our client needs to create a simulation to test an electronic kanban solution using the database's triggers, stored procedures, stored functions and views to model business/production line workflow.

## Requirements/Prerequisites
### Objectives
1. To develop a simulation solution based on SQL Server
2. To create and use triggers, stored procedures, stored functions and visualizations to model the production / business line workflow

### Solution startup order.
This solution is ready to be configured in a distributed network, as long as computers can connect to the database. The database toils as the central point of this solution. 

1. Reset the database with initial data. see [the database doc](./Documentation/projectDatabase.md).
2. Run the [ConfigurationTool](./Documentation/projectConfigurationTool.md) project to set new the values, if necessary.
3. Run the [Runner](./Documentation/projectRunner.md) project, connect and start it.
4. Run the Station project ([FLStation](./Documentation/projectStation.md)), as many instances as you want. Select a worker type, and start the station.
5. Run the [AssemblyLineKanban](./Documentation/projectAssemblyLineKanban.md) project, connect and start it.
6. Run the Andon project ([FLAndon](./Documentation/projectAndon.md)) and select a station to have its current status.  

## Built With
* [SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-downloads) - SQL Server on-premises
* [SQL Server Docker](https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-docker?view=sql-server-ver15&pivots=cs1-bash) - SQL Server on-premises in a Docker container
* [SQL Server Docker Compose](https://forums.docker.com/t/ms-sql-server-with-docker-volumes/97407) - SQL Server on-premises in a Docker container - compose cfg    
* [C Sharp](https://docs.microsoft.com/en-us/dotnet/csharp/) - C# language
* [Windows Forms](https://docs.microsoft.com/en-us/dotnet/desktop/winforms/?view=netframeworkdesktop-4.8) - Graphical user interface (GUI)

## Screenshot
Solution screenshot
![alt text](Documentation/imgs/Kanban_ScreenShot.JPG)

## Things to be improved
- Windows forms layout [ref](https://trydesignlab.com/blog/ui-design-best-practices-form-design-ux-part-1/).
- Create unit tests, [ref](https://docs.microsoft.com/en-us/appcenter/test-cloud/). 
- Create Integration tests
- Deep implementation of the MVP (Model View Presenter) design pattern [ref](https://docs.microsoft.com/en-us/archive/msdn-magazine/2006/august/design-patterns-model-view-presenter).
- Centralize environment variables, [ref](https://docs.microsoft.com/en-us/dotnet/api/system.environment.setenvironmentvariable?view=net-5.0).
- The Station project design allows spawn multiple new workers 
- Error return improvement.
- Create a log project to connect with remote log services through the network.


## Authors
* **Petterson Amaral** - *GitLab page* - [GitLab](https://gitlab.com/pettersonamaral)
* **Paulo Casado** 
  
## Kanban Explanation 

* [Kanban](http://www.strategosinc.com/kanban_1.htm) - Kanban is a system that controls flow
* [Kanban Pizza](https://youtu.be/Mdyyyu41dZ4) - Youtube video
* [Kanban for software development – For interest](https://youtu.be/R8dYLbJiTUE) - Youtube video

## License
This project is licensed under the MIT License

*Document template source page* - [Billie Thompson](https://gist.githubusercontent.com/PurpleBooth/109311bb0361f32d87a2/raw/8254b53ab8dcb18afc64287aaddd9e5b6059f880/README-Template.md)

