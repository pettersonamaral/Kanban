﻿namespace SQL_FinalProject
{
    partial class FormConfigurationTool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConfigurationTool));
            this.grpBoxSource = new System.Windows.Forms.GroupBox();
            this.picConnected = new System.Windows.Forms.PictureBox();
            this.grpBoxSourceSummary = new System.Windows.Forms.GroupBox();
            this.lblSourceDbNameStatus = new System.Windows.Forms.Label();
            this.lblSourceConnectionStatus = new System.Windows.Forms.Label();
            this.btnSourceConnection = new System.Windows.Forms.Button();
            this.tCtrlKanbanTable = new System.Windows.Forms.TabControl();
            this.tabGenaral = new System.Windows.Forms.TabPage();
            this.dgrvConfig = new System.Windows.Forms.DataGridView();
            this.configBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sQL_FinalProject_KanbanDataSet = new SQL_FinalProject.SQL_FinalProject_KanbanDataSet();
            this.btnGeneralSave = new System.Windows.Forms.Button();
            this.tabProducts = new System.Windows.Forms.TabPage();
            this.btnProductSave = new System.Windows.Forms.Button();
            this.dgrvProducts = new System.Windows.Forms.DataGridView();
            this.tblPartBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sQL_FinalProject_KanbanDataSet1 = new SQL_FinalProject.SQL_FinalProject_KanbanDataSet1();
            this.tabProfiles = new System.Windows.Forms.TabPage();
            this.btnProfileSave = new System.Windows.Forms.Button();
            this.dgrvProfiles = new System.Windows.Forms.DataGridView();
            this.tblWorkerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sQL_FinalProject_KanbanDataSet2 = new SQL_FinalProject.SQL_FinalProject_KanbanDataSet2();
            this.tblPartTableAdapter = new SQL_FinalProject.SQL_FinalProject_KanbanDataSet1TableAdapters.tblPartTableAdapter();
            this.tblWorkerTableAdapter = new SQL_FinalProject.SQL_FinalProject_KanbanDataSet2TableAdapters.tblWorkerTableAdapter();
            this.configTableAdapter = new SQL_FinalProject.SQL_FinalProject_KanbanDataSetTableAdapters.configTableAdapter();
            this.tabWelcome = new System.Windows.Forms.TabPage();
            this.lblWelcomeTxt = new System.Windows.Forms.Label();
            this.partIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.partNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxBinQtyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minBinQtyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.confignameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datatypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workerIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workerExperienceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.avgAssemblyTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.avgDefectOcurrenceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpBoxSource.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picConnected)).BeginInit();
            this.grpBoxSourceSummary.SuspendLayout();
            this.tCtrlKanbanTable.SuspendLayout();
            this.tabGenaral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrvConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.configBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sQL_FinalProject_KanbanDataSet)).BeginInit();
            this.tabProducts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrvProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPartBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sQL_FinalProject_KanbanDataSet1)).BeginInit();
            this.tabProfiles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrvProfiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblWorkerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sQL_FinalProject_KanbanDataSet2)).BeginInit();
            this.tabWelcome.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpBoxSource
            // 
            this.grpBoxSource.Controls.Add(this.picConnected);
            this.grpBoxSource.Controls.Add(this.grpBoxSourceSummary);
            this.grpBoxSource.Controls.Add(this.btnSourceConnection);
            this.grpBoxSource.Location = new System.Drawing.Point(12, 12);
            this.grpBoxSource.Name = "grpBoxSource";
            this.grpBoxSource.Size = new System.Drawing.Size(271, 124);
            this.grpBoxSource.TabIndex = 4;
            this.grpBoxSource.TabStop = false;
            this.grpBoxSource.Text = "Control Commands";
            // 
            // picConnected
            // 
            this.picConnected.Enabled = false;
            this.picConnected.Image = global::SQL_FinalProject.Properties.Resources.connected;
            this.picConnected.InitialImage = global::SQL_FinalProject.Properties.Resources.connected;
            this.picConnected.Location = new System.Drawing.Point(105, 20);
            this.picConnected.Name = "picConnected";
            this.picConnected.Size = new System.Drawing.Size(30, 27);
            this.picConnected.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picConnected.TabIndex = 3;
            this.picConnected.TabStop = false;
            this.picConnected.Visible = false;
            // 
            // grpBoxSourceSummary
            // 
            this.grpBoxSourceSummary.Controls.Add(this.lblSourceDbNameStatus);
            this.grpBoxSourceSummary.Controls.Add(this.lblSourceConnectionStatus);
            this.grpBoxSourceSummary.Location = new System.Drawing.Point(6, 53);
            this.grpBoxSourceSummary.Name = "grpBoxSourceSummary";
            this.grpBoxSourceSummary.Size = new System.Drawing.Size(259, 64);
            this.grpBoxSourceSummary.TabIndex = 2;
            this.grpBoxSourceSummary.TabStop = false;
            this.grpBoxSourceSummary.Text = "Source connection";
            // 
            // lblSourceDbNameStatus
            // 
            this.lblSourceDbNameStatus.AutoSize = true;
            this.lblSourceDbNameStatus.Location = new System.Drawing.Point(7, 42);
            this.lblSourceDbNameStatus.Name = "lblSourceDbNameStatus";
            this.lblSourceDbNameStatus.Size = new System.Drawing.Size(0, 13);
            this.lblSourceDbNameStatus.TabIndex = 2;
            // 
            // lblSourceConnectionStatus
            // 
            this.lblSourceConnectionStatus.AutoSize = true;
            this.lblSourceConnectionStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSourceConnectionStatus.Location = new System.Drawing.Point(7, 20);
            this.lblSourceConnectionStatus.Name = "lblSourceConnectionStatus";
            this.lblSourceConnectionStatus.Size = new System.Drawing.Size(0, 13);
            this.lblSourceConnectionStatus.TabIndex = 0;
            // 
            // btnSourceConnection
            // 
            this.btnSourceConnection.Location = new System.Drawing.Point(6, 19);
            this.btnSourceConnection.Name = "btnSourceConnection";
            this.btnSourceConnection.Size = new System.Drawing.Size(92, 28);
            this.btnSourceConnection.TabIndex = 0;
            this.btnSourceConnection.Text = "Connection...";
            this.btnSourceConnection.UseVisualStyleBackColor = true;
            this.btnSourceConnection.Click += new System.EventHandler(this.BtnSourceConnection_Click);
            // 
            // tCtrlKanbanTable
            // 
            this.tCtrlKanbanTable.Controls.Add(this.tabWelcome);
            this.tCtrlKanbanTable.Controls.Add(this.tabGenaral);
            this.tCtrlKanbanTable.Controls.Add(this.tabProducts);
            this.tCtrlKanbanTable.Controls.Add(this.tabProfiles);
            this.tCtrlKanbanTable.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tCtrlKanbanTable.Location = new System.Drawing.Point(0, 151);
            this.tCtrlKanbanTable.Name = "tCtrlKanbanTable";
            this.tCtrlKanbanTable.SelectedIndex = 0;
            this.tCtrlKanbanTable.Size = new System.Drawing.Size(521, 299);
            this.tCtrlKanbanTable.TabIndex = 5;
            // 
            // tabGenaral
            // 
            this.tabGenaral.Controls.Add(this.dgrvConfig);
            this.tabGenaral.Controls.Add(this.btnGeneralSave);
            this.tabGenaral.Location = new System.Drawing.Point(4, 22);
            this.tabGenaral.Name = "tabGenaral";
            this.tabGenaral.Padding = new System.Windows.Forms.Padding(3);
            this.tabGenaral.Size = new System.Drawing.Size(513, 273);
            this.tabGenaral.TabIndex = 0;
            this.tabGenaral.Text = "General";
            this.tabGenaral.UseVisualStyleBackColor = true;
            // 
            // dgrvConfig
            // 
            this.dgrvConfig.AllowUserToAddRows = false;
            this.dgrvConfig.AllowUserToDeleteRows = false;
            this.dgrvConfig.AutoGenerateColumns = false;
            this.dgrvConfig.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrvConfig.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.confignameDataGridViewTextBoxColumn,
            this.valueDataGridViewTextBoxColumn,
            this.datatypeDataGridViewTextBoxColumn});
            this.dgrvConfig.DataSource = this.configBindingSource;
            this.dgrvConfig.Location = new System.Drawing.Point(0, 0);
            this.dgrvConfig.Name = "dgrvConfig";
            this.dgrvConfig.Size = new System.Drawing.Size(413, 273);
            this.dgrvConfig.TabIndex = 10;
            // 
            // configBindingSource
            // 
            this.configBindingSource.DataMember = "config";
            this.configBindingSource.DataSource = this.sQL_FinalProject_KanbanDataSet;
            // 
            // sQL_FinalProject_KanbanDataSet
            // 
            this.sQL_FinalProject_KanbanDataSet.DataSetName = "SQL_FinalProject_KanbanDataSet";
            this.sQL_FinalProject_KanbanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnGeneralSave
            // 
            this.btnGeneralSave.Location = new System.Drawing.Point(430, 242);
            this.btnGeneralSave.Name = "btnGeneralSave";
            this.btnGeneralSave.Size = new System.Drawing.Size(75, 23);
            this.btnGeneralSave.TabIndex = 8;
            this.btnGeneralSave.Text = "Save";
            this.btnGeneralSave.UseVisualStyleBackColor = true;
            this.btnGeneralSave.Click += new System.EventHandler(this.BtnGeneralSave_Click);
            // 
            // tabProducts
            // 
            this.tabProducts.Controls.Add(this.btnProductSave);
            this.tabProducts.Controls.Add(this.dgrvProducts);
            this.tabProducts.Location = new System.Drawing.Point(4, 22);
            this.tabProducts.Name = "tabProducts";
            this.tabProducts.Padding = new System.Windows.Forms.Padding(3);
            this.tabProducts.Size = new System.Drawing.Size(513, 273);
            this.tabProducts.TabIndex = 1;
            this.tabProducts.Text = "Products";
            this.tabProducts.UseVisualStyleBackColor = true;
            // 
            // btnProductSave
            // 
            this.btnProductSave.Location = new System.Drawing.Point(430, 242);
            this.btnProductSave.Name = "btnProductSave";
            this.btnProductSave.Size = new System.Drawing.Size(75, 23);
            this.btnProductSave.TabIndex = 10;
            this.btnProductSave.Text = "Save";
            this.btnProductSave.UseVisualStyleBackColor = true;
            this.btnProductSave.Click += new System.EventHandler(this.BtnProductSave_Click);
            // 
            // dgrvProducts
            // 
            this.dgrvProducts.AllowUserToAddRows = false;
            this.dgrvProducts.AllowUserToDeleteRows = false;
            this.dgrvProducts.AutoGenerateColumns = false;
            this.dgrvProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrvProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.partIDDataGridViewTextBoxColumn,
            this.partNameDataGridViewTextBoxColumn,
            this.maxBinQtyDataGridViewTextBoxColumn,
            this.minBinQtyDataGridViewTextBoxColumn});
            this.dgrvProducts.DataSource = this.tblPartBindingSource;
            this.dgrvProducts.Location = new System.Drawing.Point(0, 0);
            this.dgrvProducts.Name = "dgrvProducts";
            this.dgrvProducts.Size = new System.Drawing.Size(413, 273);
            this.dgrvProducts.TabIndex = 9;
            // 
            // tblPartBindingSource
            // 
            this.tblPartBindingSource.DataMember = "tblPart";
            this.tblPartBindingSource.DataSource = this.sQL_FinalProject_KanbanDataSet1;
            // 
            // sQL_FinalProject_KanbanDataSet1
            // 
            this.sQL_FinalProject_KanbanDataSet1.DataSetName = "SQL_FinalProject_KanbanDataSet1";
            this.sQL_FinalProject_KanbanDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabProfiles
            // 
            this.tabProfiles.Controls.Add(this.btnProfileSave);
            this.tabProfiles.Controls.Add(this.dgrvProfiles);
            this.tabProfiles.Location = new System.Drawing.Point(4, 22);
            this.tabProfiles.Name = "tabProfiles";
            this.tabProfiles.Padding = new System.Windows.Forms.Padding(3);
            this.tabProfiles.Size = new System.Drawing.Size(513, 273);
            this.tabProfiles.TabIndex = 2;
            this.tabProfiles.Text = "Profiles";
            this.tabProfiles.UseVisualStyleBackColor = true;
            // 
            // btnProfileSave
            // 
            this.btnProfileSave.Location = new System.Drawing.Point(430, 242);
            this.btnProfileSave.Name = "btnProfileSave";
            this.btnProfileSave.Size = new System.Drawing.Size(75, 23);
            this.btnProfileSave.TabIndex = 10;
            this.btnProfileSave.Text = "Save";
            this.btnProfileSave.UseVisualStyleBackColor = true;
            this.btnProfileSave.Click += new System.EventHandler(this.BtnProfileSave_Click);
            // 
            // dgrvProfiles
            // 
            this.dgrvProfiles.AllowUserToAddRows = false;
            this.dgrvProfiles.AllowUserToDeleteRows = false;
            this.dgrvProfiles.AutoGenerateColumns = false;
            this.dgrvProfiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrvProfiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.workerIDDataGridViewTextBoxColumn,
            this.workerExperienceDataGridViewTextBoxColumn,
            this.avgAssemblyTimeDataGridViewTextBoxColumn,
            this.avgDefectOcurrenceDataGridViewTextBoxColumn});
            this.dgrvProfiles.DataSource = this.tblWorkerBindingSource;
            this.dgrvProfiles.Location = new System.Drawing.Point(0, 0);
            this.dgrvProfiles.Name = "dgrvProfiles";
            this.dgrvProfiles.Size = new System.Drawing.Size(413, 273);
            this.dgrvProfiles.TabIndex = 9;
            // 
            // tblWorkerBindingSource
            // 
            this.tblWorkerBindingSource.DataMember = "tblWorker";
            this.tblWorkerBindingSource.DataSource = this.sQL_FinalProject_KanbanDataSet2;
            // 
            // sQL_FinalProject_KanbanDataSet2
            // 
            this.sQL_FinalProject_KanbanDataSet2.DataSetName = "SQL_FinalProject_KanbanDataSet2";
            this.sQL_FinalProject_KanbanDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tblPartTableAdapter
            // 
            this.tblPartTableAdapter.ClearBeforeFill = true;
            // 
            // tblWorkerTableAdapter
            // 
            this.tblWorkerTableAdapter.ClearBeforeFill = true;
            // 
            // configTableAdapter
            // 
            this.configTableAdapter.ClearBeforeFill = true;
            // 
            // tabWelcome
            // 
            this.tabWelcome.Controls.Add(this.lblWelcomeTxt);
            this.tabWelcome.Location = new System.Drawing.Point(4, 22);
            this.tabWelcome.Name = "tabWelcome";
            this.tabWelcome.Padding = new System.Windows.Forms.Padding(3);
            this.tabWelcome.Size = new System.Drawing.Size(513, 273);
            this.tabWelcome.TabIndex = 3;
            this.tabWelcome.Text = "Welcome";
            this.tabWelcome.UseVisualStyleBackColor = true;
            // 
            // lblWelcomeTxt
            // 
            this.lblWelcomeTxt.AutoSize = true;
            this.lblWelcomeTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcomeTxt.Location = new System.Drawing.Point(68, 87);
            this.lblWelcomeTxt.Name = "lblWelcomeTxt";
            this.lblWelcomeTxt.Size = new System.Drawing.Size(368, 16);
            this.lblWelcomeTxt.TabIndex = 0;
            this.lblWelcomeTxt.Text = "To start the Configuration Tool, click on \"Connection...\" button.";
            // 
            // partIDDataGridViewTextBoxColumn
            // 
            this.partIDDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.partIDDataGridViewTextBoxColumn.DataPropertyName = "PartID";
            this.partIDDataGridViewTextBoxColumn.HeaderText = "PartID";
            this.partIDDataGridViewTextBoxColumn.Name = "partIDDataGridViewTextBoxColumn";
            this.partIDDataGridViewTextBoxColumn.Visible = false;
            this.partIDDataGridViewTextBoxColumn.Width = 62;
            // 
            // partNameDataGridViewTextBoxColumn
            // 
            this.partNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.partNameDataGridViewTextBoxColumn.DataPropertyName = "PartName";
            this.partNameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.partNameDataGridViewTextBoxColumn.Name = "partNameDataGridViewTextBoxColumn";
            this.partNameDataGridViewTextBoxColumn.Width = 60;
            // 
            // maxBinQtyDataGridViewTextBoxColumn
            // 
            this.maxBinQtyDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.maxBinQtyDataGridViewTextBoxColumn.DataPropertyName = "MaxBinQty";
            this.maxBinQtyDataGridViewTextBoxColumn.HeaderText = "Max bin qty";
            this.maxBinQtyDataGridViewTextBoxColumn.Name = "maxBinQtyDataGridViewTextBoxColumn";
            this.maxBinQtyDataGridViewTextBoxColumn.Width = 86;
            // 
            // minBinQtyDataGridViewTextBoxColumn
            // 
            this.minBinQtyDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.minBinQtyDataGridViewTextBoxColumn.DataPropertyName = "MinBinQty";
            this.minBinQtyDataGridViewTextBoxColumn.HeaderText = "Min bin qty";
            this.minBinQtyDataGridViewTextBoxColumn.Name = "minBinQtyDataGridViewTextBoxColumn";
            this.minBinQtyDataGridViewTextBoxColumn.Width = 83;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            this.idDataGridViewTextBoxColumn.Width = 40;
            // 
            // confignameDataGridViewTextBoxColumn
            // 
            this.confignameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.confignameDataGridViewTextBoxColumn.DataPropertyName = "config_name";
            this.confignameDataGridViewTextBoxColumn.HeaderText = "Config name";
            this.confignameDataGridViewTextBoxColumn.Name = "confignameDataGridViewTextBoxColumn";
            this.confignameDataGridViewTextBoxColumn.Width = 91;
            // 
            // valueDataGridViewTextBoxColumn
            // 
            this.valueDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.valueDataGridViewTextBoxColumn.DataPropertyName = "value";
            this.valueDataGridViewTextBoxColumn.HeaderText = "Value";
            this.valueDataGridViewTextBoxColumn.Name = "valueDataGridViewTextBoxColumn";
            this.valueDataGridViewTextBoxColumn.Width = 59;
            // 
            // datatypeDataGridViewTextBoxColumn
            // 
            this.datatypeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.datatypeDataGridViewTextBoxColumn.DataPropertyName = "data_type";
            this.datatypeDataGridViewTextBoxColumn.HeaderText = "Data type";
            this.datatypeDataGridViewTextBoxColumn.Name = "datatypeDataGridViewTextBoxColumn";
            this.datatypeDataGridViewTextBoxColumn.Width = 78;
            // 
            // workerIDDataGridViewTextBoxColumn
            // 
            this.workerIDDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.workerIDDataGridViewTextBoxColumn.DataPropertyName = "WorkerID";
            this.workerIDDataGridViewTextBoxColumn.HeaderText = "WorkerID";
            this.workerIDDataGridViewTextBoxColumn.Name = "workerIDDataGridViewTextBoxColumn";
            this.workerIDDataGridViewTextBoxColumn.Visible = false;
            this.workerIDDataGridViewTextBoxColumn.Width = 78;
            // 
            // workerExperienceDataGridViewTextBoxColumn
            // 
            this.workerExperienceDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.workerExperienceDataGridViewTextBoxColumn.DataPropertyName = "WorkerExperience";
            this.workerExperienceDataGridViewTextBoxColumn.HeaderText = "Experience level";
            this.workerExperienceDataGridViewTextBoxColumn.Name = "workerExperienceDataGridViewTextBoxColumn";
            this.workerExperienceDataGridViewTextBoxColumn.Width = 101;
            // 
            // avgAssemblyTimeDataGridViewTextBoxColumn
            // 
            this.avgAssemblyTimeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.avgAssemblyTimeDataGridViewTextBoxColumn.DataPropertyName = "AvgAssemblyTime";
            this.avgAssemblyTimeDataGridViewTextBoxColumn.HeaderText = "Avg. assembly time";
            this.avgAssemblyTimeDataGridViewTextBoxColumn.Name = "avgAssemblyTimeDataGridViewTextBoxColumn";
            this.avgAssemblyTimeDataGridViewTextBoxColumn.Width = 112;
            // 
            // avgDefectOcurrenceDataGridViewTextBoxColumn
            // 
            this.avgDefectOcurrenceDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.avgDefectOcurrenceDataGridViewTextBoxColumn.DataPropertyName = "AvgDefectOcurrence";
            this.avgDefectOcurrenceDataGridViewTextBoxColumn.HeaderText = "Avg. defect occurrence";
            this.avgDefectOcurrenceDataGridViewTextBoxColumn.Name = "avgDefectOcurrenceDataGridViewTextBoxColumn";
            this.avgDefectOcurrenceDataGridViewTextBoxColumn.Width = 132;
            // 
            // FormConfigurationTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 450);
            this.Controls.Add(this.tCtrlKanbanTable);
            this.Controls.Add(this.grpBoxSource);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormConfigurationTool";
            this.Text = "ConfigurationTool";
            this.Load += new System.EventHandler(this.FormConfigurationTool_Load);
            this.grpBoxSource.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picConnected)).EndInit();
            this.grpBoxSourceSummary.ResumeLayout(false);
            this.grpBoxSourceSummary.PerformLayout();
            this.tCtrlKanbanTable.ResumeLayout(false);
            this.tabGenaral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgrvConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.configBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sQL_FinalProject_KanbanDataSet)).EndInit();
            this.tabProducts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgrvProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblPartBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sQL_FinalProject_KanbanDataSet1)).EndInit();
            this.tabProfiles.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgrvProfiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblWorkerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sQL_FinalProject_KanbanDataSet2)).EndInit();
            this.tabWelcome.ResumeLayout(false);
            this.tabWelcome.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpBoxSource;
        private System.Windows.Forms.GroupBox grpBoxSourceSummary;
        private System.Windows.Forms.Label lblSourceDbNameStatus;
        private System.Windows.Forms.Label lblSourceConnectionStatus;
        private System.Windows.Forms.Button btnSourceConnection;
        private System.Windows.Forms.TabControl tCtrlKanbanTable;
        private System.Windows.Forms.TabPage tabGenaral;
        private System.Windows.Forms.TabPage tabProducts;
        private System.Windows.Forms.PictureBox picConnected;
        private System.Windows.Forms.TabPage tabProfiles;
        private System.Windows.Forms.Button btnGeneralSave;
        private System.Windows.Forms.Button btnProductSave;
        private System.Windows.Forms.DataGridView dgrvProducts;
        private System.Windows.Forms.Button btnProfileSave;
        private System.Windows.Forms.DataGridView dgrvProfiles;


        private SQL_FinalProject_KanbanDataSet1 sQL_FinalProject_KanbanDataSet1;
        private System.Windows.Forms.BindingSource tblPartBindingSource;
        private SQL_FinalProject_KanbanDataSet1TableAdapters.tblPartTableAdapter tblPartTableAdapter;


        private SQL_FinalProject_KanbanDataSet2 sQL_FinalProject_KanbanDataSet2;
        private System.Windows.Forms.BindingSource tblWorkerBindingSource;
        private SQL_FinalProject_KanbanDataSet2TableAdapters.tblWorkerTableAdapter tblWorkerTableAdapter;
        private System.Windows.Forms.DataGridView dgrvConfig;


        private SQL_FinalProject_KanbanDataSet sQL_FinalProject_KanbanDataSet;
        private System.Windows.Forms.BindingSource configBindingSource;
        private SQL_FinalProject_KanbanDataSetTableAdapters.configTableAdapter configTableAdapter;
        private System.Windows.Forms.TabPage tabWelcome;
        private System.Windows.Forms.Label lblWelcomeTxt;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn confignameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datatypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn partIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn partNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxBinQtyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn minBinQtyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerExperienceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn avgAssemblyTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn avgDefectOcurrenceDataGridViewTextBoxColumn;
    }
}

