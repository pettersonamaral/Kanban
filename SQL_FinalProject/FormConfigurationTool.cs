﻿/*****************************************************************************************************
* FILE : 		  Program.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Mar-30


* DESCRIPTION/Requirements: 
1. To develop a SQL-based simulation solution
2. To create and use triggers, stored procedures, stored functions and views to model 
   business/production line workflow

   The customer builds a fog lamp assembly for automobiles. The assembly is made up of a harness, a 
   reflector, a housing, a lens, a bulb and a bezel. Each of these parts is in its bin, and an 
   employee takes one of each of these parts and puts it together. Once completed, the fog lamp is 
   placed on a tray with separators. When full, the tray is sent to the testing department (e.g. 
   these are the ‘test trays’ mentioned below).

* Refence/sources:

*****************************************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQL_FinalProject
{
    public partial class FormConfigurationTool : Form
    {
        private Database myDatabase = Database.Instance;                    //Load/Get Database Class instance

        public FormConfigurationTool()
        {
            InitializeComponent();

            if (!myDatabase.ConnectionStatus)                               // If myDatabase is NOT connected
            {
                ItIsDisconnected();
            }

        }

        /// <summary>
        /// Button Connection. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSourceConnection_Click(object sender, EventArgs e)
        {
            try
            {
                if (myDatabase.ConnectionStatus)                            // If myDatabase is already connected
                {
                    if (myDatabase.Disconnect())                            // If set up a new connection is true
                    {
                        ItIsDisconnected();
                    }
                }
                else                                                        // If myDatabase is NOT connected
                {
                    if (myDatabase.Connect())                               // If set up a new connection is return true
                    {
                        ItIsConnected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Exception!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        /// <summary>
        /// Support method to Connected status true
        /// </summary>
        private void ItIsConnected()
        {
            lblSourceConnectionStatus.Text = Database.CONNECTEDSTATUS;
            lblSourceDbNameStatus.Text = Database.DBNAME + myDatabase.DbName.ToString();
            picConnected.Visible = true;
            myDatabase.ConnectionStatus = true;
            tCtrlKanbanTable.Enabled = true;
            tCtrlKanbanTable.SelectedIndex = 1;                 //Jump to first config tab
            workerIDDataGridViewTextBoxColumn.Visible = false;  //Hides Id column
            partIDDataGridViewTextBoxColumn.Visible = false;    //Hides Id column
            idDataGridViewTextBoxColumn.Visible = false;        //Hides Id column
        }


        /// <summary>
        /// Support method to Connected status false
        /// </summary>
        private void ItIsDisconnected()
        {
            lblSourceConnectionStatus.Text = Database.DISCONNECTEDSTATUS;
            lblSourceDbNameStatus.Text = "";
            picConnected.Visible = false;
            myDatabase.ConnectionStatus = false;
            tCtrlKanbanTable.SelectedIndex = 0;                 //Jump to welcome tab
            tCtrlKanbanTable.Enabled = false;        
        }


        /// <summary>
        /// Saves the data after the change.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnGeneralSave_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult saveGeneralData = MessageBox.Show("Are you sure that you want to save these changes?", "Save changes?", MessageBoxButtons.YesNo);
                if (saveGeneralData == DialogResult.Yes)
                {
                    dgrvConfig.EndEdit();
                    configTableAdapter.Update(sQL_FinalProject_KanbanDataSet);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Failure!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        /// <summary>
        /// Saves the data after the change.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnProductSave_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult saveGeneralData = MessageBox.Show("Are you sure that you want to save these changes?", "Save changes?", MessageBoxButtons.YesNo);
                if (saveGeneralData == DialogResult.Yes)
                {
                    dgrvProducts.EndEdit();
                    tblPartTableAdapter.Update(sQL_FinalProject_KanbanDataSet1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Failure!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        /// <summary>
        /// Saves the data after the change.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnProfileSave_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult saveGeneralData = MessageBox.Show("Are you sure that you want to save these changes?", "Save changes?", MessageBoxButtons.YesNo);
                if (saveGeneralData == DialogResult.Yes)
                {
                    dgrvProfiles.EndEdit();
                    tblWorkerTableAdapter.Update(sQL_FinalProject_KanbanDataSet2);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Failure!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        private void FormConfigurationTool_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'sQL_FinalProject_KanbanDataSet.config' table. You can move, or remove it, as needed.
            this.configTableAdapter.Fill(this.sQL_FinalProject_KanbanDataSet.config);

            // TODO: This line of code loads data into the 'sQL_FinalProject_KanbanDataSet2.tblWorker' table. You can move, or remove it, as needed.
            this.tblWorkerTableAdapter.Fill(this.sQL_FinalProject_KanbanDataSet2.tblWorker);

            // TODO: This line of code loads data into the 'sQL_FinalProject_KanbanDataSet1.tblPart' table. You can move, or remove it, as needed.
            this.tblPartTableAdapter.Fill(this.sQL_FinalProject_KanbanDataSet1.tblPart);
        }
    }
}
