﻿/*****************************************************************************************************
* FILE : 		  Database.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Mar-30


* DESCRIPTION/Requirements: 
    Create and suport database connection and more.
    * Implementing Singleton in C# - Implementation Strategy - Multithreaded Singleton
    
* Refence/sources:
https://msdn.microsoft.com/en-us/library/ff650316.aspx
*****************************************************************************************************/
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace SQL_FinalProject
{
    public sealed class Database
    {
        public const String CONNECTEDSTATUS     = "Connected";
        public const String DISCONNECTEDSTATUS  = "Disconnected";
        public const String DBNAME              = "Database name: ";

        #region Properties
        public bool ConnectionStatus    { get; set; }                                           //Connection Status either Connected = True or Disconnected = False
        public DataTable ChartSource    { get; set; }                                           //Chart Source
        public DataTable SelectClause   { get; set; }                                           //Datareader
        public String DbName            { get; set; }                                           //Database name
        public String HostDBService     { get; set; }                                           //Hold the current connection provider
        public SqlConnection Conn       { get; set; }                                           //Database Connection 
        #endregion

        #region Attributes
        private SqlCommand cmd = new SqlCommand();                                              //Database Command

        private static volatile Database instance;                                              //Implementation Strategy - Multithreaded Singleton
        private static object syncRoot = new Object();                                          //Implementation Strategy - Multithreaded Singleton
        #endregion

        /// <summary>
        /// Default constructor
        /// </summary>
        private Database() { ConnectionStatus = false; }


        #region Methods

        // --- Starts Singleton Pattern
        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        public static Database Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new Database();
                    }
                }
                return instance;
            }
        }   
        // --- ends Singleton Pattern


        /// <summary>
        /// Method to handle the connection 
        /// </summary>       
        /// <returns>bool</returns>
        public bool Connect()
        {
            bool setReturMethod = false;
            ConnectionStatus = false;
            Conn = new SqlConnection();


            try
            {
                //String strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["SQL_FinalProject_Kanban"].ConnectionString;
                String strConnection = Helper.ConnectionVal("SQL_FinalProject_Kanban");

                Conn.ConnectionString = strConnection;
                DbName = Conn.Database.ToString();
                setReturMethod = true;
                ConnectionStatus = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Exception!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return setReturMethod;
        }


        /// <summary>
        /// Method to handle the disconnection
        /// </summary>
        /// <returns></returns>
        public bool Disconnect()
        {
            bool setReturMethod = false;

            try
            {
                Conn.Dispose();
                cmd.Dispose();
                ConnectionStatus = false;
                setReturMethod = true;
                HostDBService = "";
                DbName = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Exception!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return setReturMethod;
        }
        #endregion

    }
}
