﻿/*****************************************************************************************************
* FILE : 		  ConfigTable.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Mar-30


* DESCRIPTION/Requirements: 
    Config table content 

* Refence/sources:
* 
*****************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_FinalProject
{
    class ConfigTable
    {
        public Int32 Config_id { get; set; }
        public String Config_name { get; set; }
        public String Config_value { get; set; }
        public String Config_datatype { get; set; }
    }
}
