﻿/*****************************************************************************************************
* FILE : 		  Helper.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Mar-30


* DESCRIPTION/Requirements: 
    1. This Class returns strings. 

* Refence/sources:
* https://www.youtube.com/watch?v=Et2khGnrIqc
* https://www.youtube.com/watch?v=pBCPc44CE74
*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_FinalProject
{
    public static class Helper
    {
        /// <summary>
        /// This method returns the connection string based on a connection name
        /// </summary>
        /// <param name="val"> Connection name </param>
        /// <returns></returns>
        public static String ConnectionVal(String val)
        {
            //SQL_FinalProject_Kanban
            return ConfigurationManager.ConnectionStrings[val].ConnectionString;
        }


        /// <summary>
        /// Return a SQL command
        /// </summary>
        /// <returns></returns>
        public static String GetSelectTimeScale()
        {
            String setReturMethod = "SELECT value, data_type FROM config WHERE config_name = 'speed_time'";

            return setReturMethod; 
        }


        /// <summary>
        /// Return a SQL command
        /// </summary>
        /// <returns></returns>
        public static String GetSelectRunnerTime()
        {
            String setReturMethod = "SELECT value, data_type FROM config WHERE config_name = 'runner_time'";

            return setReturMethod;
        }


        /// <summary>
        /// Return a AssemblyLine timer
        /// </summary>
        /// <returns></returns>
        public static String GetAssemblyLineTimer()
        {
            String setReturMethod = "SELECT value, data_type FROM config WHERE config_name = 'runner_time'";

            return setReturMethod;
        }


        /// <summary>
        /// Return a SQL command
        /// </summary>
        /// <returns></returns>
        public static String GetStoredProcedureRunRunner()
        {
            String setReturMethod = "runner_running";

            return setReturMethod;
        }


    }
}
