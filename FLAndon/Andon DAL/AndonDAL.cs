﻿/*****************************************************************************************************
* FILE : 		  AndonDAL.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-16


* DESCRIPTION/Requirements: 
    Data Access Layer, access the database based on requests of the Business Layer
    
* Refence/sources:
*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SQL_FinalProject;

namespace FLAndon
{
    public static class AndonDAL
    {
        #region Properties

        // Properties
        static SqlConnection conn;
        private static Database theDatabase = Database.Instance;

        #endregion Properties
        
        #region Methods

        // DATABASE CONNECTION SETUP ===============================================================================================================

        /// <summary>
        /// Setup database connection based on AppConfig connection String
        /// </summary>
        /// <returns></returns>
        public static SqlConnection SetDBConnection()
        {
            // Setup Connection with database
            conn = new SqlConnection();
            try
            {
                conn.ConnectionString =
                    ConfigurationManager.ConnectionStrings["SQL_FinalProject_Kanban"].ConnectionString;
            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[ANDON] Could not connect with Database.Invalid Connection String [EXCEPTION] {0}", ex.Message);
                throw (new FLAndonException(fullException.ToString()));
            }

            return conn;
        }

        /// =========================================================================================================================================

        // ANDON SETUP ==============================================================================================================================

        /// <summary>
        /// Access stations information (tblStation) from the database
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String,String> SelectStations()
        {
            // Locals
            var allStations = new Dictionary<string, string>();
            var conn = SetDBConnection();
            var cmd = new SqlCommand();
            SqlDataReader rdr = null;

            try
            {
                // Setup COmmand
                cmd.Connection = conn;
                cmd.CommandText = "getAllStations";
                cmd.CommandType = CommandType.StoredProcedure;

                // Open Connection
                conn.Open();

                // Setup Reader
                rdr = cmd.ExecuteReader();

                // Loop through information and add to the list
                while (rdr.Read())
                {
                    // Retrieve records and and store it within a string dictionary
                    var station = $"{rdr["StationID"]}";
                    var worker = $"{rdr["WorkerExperience"]}";
                    allStations.Add(station, worker);
                }
            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[ANDON] Couldn't retrieve the Station/Worker Units [EXCEPTION] {0}", ex.Message);
                throw (new FLAndonException(fullException.ToString()));
            }
            finally
            {
                // Close Reader
                if (rdr != null && !rdr.IsClosed)
                {
                    rdr.Close();
                }

                // Close connection
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }

            }

            return allStations;

        }

        /// =========================================================================================================================================
        
        // ANDON ACTIONS ============================================================================================================================

        /// <summary>
        /// Access bin currenty qtys from database
        /// </summary>
        /// <param name="selectedPartID"></param>
        /// <returns></returns>
        public static int SelectBinCurrentQty(int stationID, int binPartID)
        {
            // Locals
            var currentPartUnits = 0;
            var conn = SetDBConnection();
            var cmd = new SqlCommand();
            SqlDataReader rdr = null;

            try
            {
                // Setup COmmand
                cmd.Connection = conn;
                cmd.CommandText = "checkBinPartsCount";
                cmd.CommandType = CommandType.StoredProcedure;

                // Add parameters
                cmd.Parameters.Add("@stationID", SqlDbType.Int).Value = stationID;
                cmd.Parameters.Add("@binPartID", SqlDbType.Int).Value = binPartID;

                // Open Connection
                conn.Open();

                // Setup Reader
                rdr = cmd.ExecuteReader();

                // Loop through information and add to the list
                while (rdr.Read()) { int.TryParse((rdr[0].ToString()), out currentPartUnits); }

            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[ANDON] Couldn't retrieve the Bin current number of parts [EXCEPTION] {0}", ex.Message);
                throw (new FLAndonException(fullException.ToString()));
            }
            finally
            {
                // Close Reader
                if (rdr != null && !rdr.IsClosed) { rdr.Close(); }

                // Close connection
                if (conn.State != ConnectionState.Closed) { conn.Close(); }
            }

            return currentPartUnits;
        }

        #endregion Methods
    }
}
