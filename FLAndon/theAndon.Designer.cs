﻿namespace FLAndon
{
    partial class theAndon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(theAndon));
            this.cbxStation = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.plHarness = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.tbHarnessQty = new System.Windows.Forms.TextBox();
            this.lblBin1Qty = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbReflectorQty = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.plReflector = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.tbHousingQty = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.plHousing = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.tbLensQty = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.plLens = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.tbBulbQty = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.plBulb = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.tbBezelQty = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.plBezel = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.tbCurrentStationID = new System.Windows.Forms.TextBox();
            this.tbCurrentWorker = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.SuspendLayout();
            // 
            // cbxStation
            // 
            this.cbxStation.FormattingEnabled = true;
            this.cbxStation.Location = new System.Drawing.Point(14, 28);
            this.cbxStation.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxStation.Name = "cbxStation";
            this.cbxStation.Size = new System.Drawing.Size(223, 21);
            this.cbxStation.TabIndex = 0;
            this.cbxStation.SelectedIndexChanged += new System.EventHandler(this.cbxStation_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Active Station";
            // 
            // plHarness
            // 
            this.plHarness.Location = new System.Drawing.Point(33, 202);
            this.plHarness.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.plHarness.Name = "plHarness";
            this.plHarness.Size = new System.Drawing.Size(76, 18);
            this.plHarness.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 183);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Harness";
            // 
            // tbHarnessQty
            // 
            this.tbHarnessQty.Location = new System.Drawing.Point(140, 202);
            this.tbHarnessQty.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbHarnessQty.Name = "tbHarnessQty";
            this.tbHarnessQty.Size = new System.Drawing.Size(78, 20);
            this.tbHarnessQty.TabIndex = 4;
            // 
            // lblBin1Qty
            // 
            this.lblBin1Qty.AutoSize = true;
            this.lblBin1Qty.Location = new System.Drawing.Point(176, 183);
            this.lblBin1Qty.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBin1Qty.Name = "lblBin1Qty";
            this.lblBin1Qty.Size = new System.Drawing.Size(41, 13);
            this.lblBin1Qty.TabIndex = 5;
            this.lblBin1Qty.Text = "Bin Qty";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(176, 228);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Bin Qty";
            // 
            // tbReflectorQty
            // 
            this.tbReflectorQty.Location = new System.Drawing.Point(140, 247);
            this.tbReflectorQty.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbReflectorQty.Name = "tbReflectorQty";
            this.tbReflectorQty.Size = new System.Drawing.Size(78, 20);
            this.tbReflectorQty.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 228);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Reflector";
            // 
            // plReflector
            // 
            this.plReflector.Location = new System.Drawing.Point(33, 247);
            this.plReflector.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.plReflector.Name = "plReflector";
            this.plReflector.Size = new System.Drawing.Size(76, 18);
            this.plReflector.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(176, 271);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Bin Qty";
            // 
            // tbHousingQty
            // 
            this.tbHousingQty.Location = new System.Drawing.Point(140, 289);
            this.tbHousingQty.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbHousingQty.Name = "tbHousingQty";
            this.tbHousingQty.Size = new System.Drawing.Size(78, 20);
            this.tbHousingQty.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 271);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Housing";
            // 
            // plHousing
            // 
            this.plHousing.Location = new System.Drawing.Point(33, 289);
            this.plHousing.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.plHousing.Name = "plHousing";
            this.plHousing.Size = new System.Drawing.Size(76, 18);
            this.plHousing.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(176, 314);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Bin Qty";
            // 
            // tbLensQty
            // 
            this.tbLensQty.Location = new System.Drawing.Point(140, 333);
            this.tbLensQty.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbLensQty.Name = "tbLensQty";
            this.tbLensQty.Size = new System.Drawing.Size(78, 20);
            this.tbLensQty.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(33, 314);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Lens";
            // 
            // plLens
            // 
            this.plLens.Location = new System.Drawing.Point(33, 333);
            this.plLens.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.plLens.Name = "plLens";
            this.plLens.Size = new System.Drawing.Size(76, 18);
            this.plLens.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(176, 361);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Bin Qty";
            // 
            // tbBulbQty
            // 
            this.tbBulbQty.Location = new System.Drawing.Point(140, 379);
            this.tbBulbQty.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbBulbQty.Name = "tbBulbQty";
            this.tbBulbQty.Size = new System.Drawing.Size(78, 20);
            this.tbBulbQty.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(33, 361);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Bulb";
            // 
            // plBulb
            // 
            this.plBulb.Location = new System.Drawing.Point(33, 379);
            this.plBulb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.plBulb.Name = "plBulb";
            this.plBulb.Size = new System.Drawing.Size(76, 18);
            this.plBulb.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(176, 405);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Bin Qty";
            // 
            // tbBezelQty
            // 
            this.tbBezelQty.Location = new System.Drawing.Point(140, 423);
            this.tbBezelQty.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbBezelQty.Name = "tbBezelQty";
            this.tbBezelQty.Size = new System.Drawing.Size(78, 20);
            this.tbBezelQty.TabIndex = 24;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(33, 405);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Bezel";
            // 
            // plBezel
            // 
            this.plBezel.Location = new System.Drawing.Point(33, 423);
            this.plBezel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.plBezel.Name = "plBezel";
            this.plBezel.Size = new System.Drawing.Size(76, 18);
            this.plBezel.TabIndex = 22;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(16, 157);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(220, 304);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Station Status";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(122, 470);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(113, 19);
            this.btnRefresh.TabIndex = 27;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // tbCurrentStationID
            // 
            this.tbCurrentStationID.Location = new System.Drawing.Point(34, 107);
            this.tbCurrentStationID.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbCurrentStationID.Name = "tbCurrentStationID";
            this.tbCurrentStationID.Size = new System.Drawing.Size(50, 20);
            this.tbCurrentStationID.TabIndex = 28;
            // 
            // tbCurrentWorker
            // 
            this.tbCurrentWorker.Location = new System.Drawing.Point(88, 107);
            this.tbCurrentWorker.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbCurrentWorker.Name = "tbCurrentWorker";
            this.tbCurrentWorker.Size = new System.Drawing.Size(130, 20);
            this.tbCurrentWorker.TabIndex = 29;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(34, 91);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(51, 13);
            this.label13.TabIndex = 30;
            this.label13.Text = "StationID";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(88, 91);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 13);
            this.label14.TabIndex = 31;
            this.label14.Text = "Worker Experience";
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(16, 64);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(220, 81);
            this.groupBox2.TabIndex = 32;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Current Selection";
            // 
            // theAndon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(250, 499);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tbCurrentWorker);
            this.Controls.Add(this.tbCurrentStationID);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbBezelQty);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.plBezel);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbBulbQty);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.plBulb);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbLensQty);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.plLens);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbHousingQty);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.plHousing);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbReflectorQty);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.plReflector);
            this.Controls.Add(this.lblBin1Qty);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.plHarness);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxStation);
            this.Controls.Add(this.tbHarnessQty);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "theAndon";
            this.Text = "Andon Display";
            this.Load += new System.EventHandler(this.theAndon_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxStation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel plHarness;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbHarnessQty;
        private System.Windows.Forms.Label lblBin1Qty;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbReflectorQty;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel plReflector;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbHousingQty;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel plHousing;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbLensQty;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel plLens;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbBulbQty;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel plBulb;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbBezelQty;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel plBezel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.TextBox tbCurrentStationID;
        private System.Windows.Forms.TextBox tbCurrentWorker;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

