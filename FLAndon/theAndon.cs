﻿/*****************************************************************************************************
* FILE : 		  theAndon.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-08


* DESCRIPTION/Requirements: 
    Manages Andon Form events based on user selections
  
    
* Refence/sources:
*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FLStation;

namespace FLAndon
{
    public partial class theAndon : Form
    {
        #region Local Attributes

        private List<PartModel> binPartsSetup;
        private List<TextBox> binsNumbersDisplay;
        private List<Panel> binsColorsDisplay;

        #endregion Local Attributes

        #region Form Methods

        public theAndon()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Makes the Andon intitial setup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void theAndon_Load(object sender, EventArgs e)
        {
            // SET STATION COMBO BOX =========================================================================================
            AndonManager.LoadActiveStations(cbxStation, tbCurrentStationID, tbCurrentWorker);

            // SET BIN ATTRIBUTES ============================================================================================
            binPartsSetup = StationManager.GetBinSetupFromConfigTable();

            // Set List of Station Bin Number Displays
            binsNumbersDisplay = new List<TextBox> { tbHarnessQty, tbReflectorQty, tbHousingQty, tbLensQty, tbBulbQty, tbBezelQty };

            // Set List of Station Bin Color Displays
            binsColorsDisplay = new List<Panel> { plHarness, plReflector, plHousing, plLens, plBulb, plBezel };
        }


        /// <summary>
        ///  Updates StationID and Worler type based on combobox selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxStation_SelectedIndexChanged(object sender, EventArgs e)
        {

            // SET STATION COMBO BOX =========================================================================================
            //AndonManager.LoadActiveStations(cbxStation, tbCurrentStationID, tbCurrentWorker);
            // get selected KVP [REFERENCE: https://stackoverflow.com/questions/23095060/how-to-get-key-from-selecteditem-of-combobox]
            KeyValuePair<String, String> selectedEntry = (KeyValuePair<String, String>)cbxStation.SelectedItem;

            ////var ID = (string)activeStations.SelectedValue;
            tbCurrentStationID.Text = selectedEntry.Key;
            tbCurrentWorker.Text = selectedEntry.Value;
        }


        /// <summary>
        /// Load the current numbers of the selected station
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            // Get selected Station ID
            var currentStationID = Convert.ToInt32(tbCurrentStationID.Text);

            StationManager.ChangeBinsDisplayNumbers(currentStationID, binPartsSetup, binsNumbersDisplay);

            CheckBinStockFlagsThroughAndon(currentStationID, binPartsSetup);
        }

        #endregion

        #region Support Methods

        /// <summary>
        ///  Check Bin Flags and change Station Display colors accordingly
        /// </summary>
        /// <param name="stationID"></param>
        /// <param name="binList"></param>
        private void CheckBinStockFlagsThroughAndon(int stationID, List<PartModel> binList)
        {
            //Locals
            try
            {
                for (int i = 0; i < binList.Count; i++)
                {
                    var binColorIndicator = StationManager.VerifyBinLineStockFlag(stationID, binList[i].PartID, binList[i].MinBinQty, binsNumbersDisplay[i]);
                    SetPanelColor(binColorIndicator, binsColorsDisplay[i]);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Changes the color of the panel
        /// </summary>
        /// <param name="indicator"></param>
        /// <param name="indicatorColor"></param>
        private void SetPanelColor(int colorIndicator, Panel binColorDisplay)
        {
            // Change the panel color based on entry values
            switch (colorIndicator)
            {
                case StationManager.BIN_NORMAL_LEVEL_CONDITION:

                    binColorDisplay.BackColor = Color.GreenYellow;
                    break;

                case StationManager.BIN_LOW_LEVEL_CONDITION:

                    binColorDisplay.BackColor = Color.Orange;
                    break;
                case StationManager.BIN_ZEROED_LEVEL_CONDITION:

                    binColorDisplay.BackColor = Color.Red;
                    break;
            }
        }

        #endregion Support Methods
    }
}
