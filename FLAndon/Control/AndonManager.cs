﻿/*****************************************************************************************************
* FILE : 		  AndonManager.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-08


* DESCRIPTION/Requirements: 
    Manages Station business rules and talks to Data Access Layer (DAL)
  
    
* Refence/sources:
*****************************************************************************************************/


using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FLStation;

namespace FLAndon
{
    public static class AndonManager
    {
        #region Methods
        

        /// <summary>
        /// Call method from DAL to get information about the stations
        /// </summary>
        /// <param name="activeStations"></param>
        public static void LoadActiveStations(ComboBox activeStations, TextBox tbCurrentStation, TextBox tbWorkerType)
        {
            // Locals
            var dtStations = new DataTable();
            var bs = new BindingSource();
            var currentStations = new Dictionary<String, String>();

            try
            {
                // Call DAL function to fill combo box with available stations
                currentStations = AndonDAL.SelectStations();

                // Bind list with combo box
                bs.DataSource = currentStations;
                activeStations.DataSource = bs;

                // Fill Current selection boxes

                // get selected KVP [REFERENCE: https://stackoverflow.com/questions/23095060/how-to-get-key-from-selecteditem-of-combobox]
                KeyValuePair<String, String> selectedEntry; 
                if ( activeStations.SelectedItem is KeyValuePair<String, String>)
                {
                    selectedEntry = (KeyValuePair<String, String>)activeStations.SelectedItem;
                    tbCurrentStation.Text = selectedEntry.Key;
                    tbWorkerType.Text = selectedEntry.Value;
                } 
                else
                {
                    MessageBox.Show("Do you need to get the stations start first", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        /// <summary>
        /// Change Andon Display textboxes according to database values
        /// </summary>
        /// <param name="stationID"></param>
        /// <param name="binList"></param>
        /// <param name="binsTextBoxes"></param>
        public static void ChangeAndonDisplayNumbers(int stationID, List<PartModel> binList, List<TextBox> andonTextBoxes)
        {
            try
            {
                for (int i = 0; i < binList.Count; i++)
                {
                    var textBoxUpdatedNumber = FLStationDAL.SelectBinCurrentNumberOfParts(stationID, binList[i].PartID);
                    andonTextBoxes[i].Text = textBoxUpdatedNumber.ToString();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        #endregion Methods
    }
}
