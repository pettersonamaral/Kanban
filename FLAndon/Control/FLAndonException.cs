﻿/*****************************************************************************************************
* FILE : 		  FLAndonDALException.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-16


* DESCRIPTION/Requirements: 
    Data Access Layer Exception, makes exceptions at this level visible
    
* Refence/sources: Advanced SQL class
*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FLAndon
{
    class FLAndonException: Exception
    {
        #region Methods

        /// <summary>
        /// Constructor
        /// </summary>
        public FLAndonException()
        {

        }

        /// <summary>
        /// Exception base
        /// </summary>
        /// <param name="message"></param>
        public FLAndonException(String message) : base(message)
        {
            // Using Norbert's example
        }

        #endregion Methods
    }
}
