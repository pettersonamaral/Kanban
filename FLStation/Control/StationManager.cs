﻿/*****************************************************************************************************
* FILE : 		  StationManager.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-08


* DESCRIPTION/Requirements: 
    Manages Station business rules and talks to Data Access Layer (DAL)
  
    
* Refence/sources:
*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using FLStation;

namespace FLStation
{   
    /// <summary>
    ///  Contains the Business logic to pass the arguments to the Data Access Layer (DAL). 
    ///  Get the requests from the form and pass the arguments...
    /// </summary>
    public static class StationManager
    {
        #region Constants

        // CONSTANTS ===============================================================================================================================

        public const int TRAY_CAPACITY = 60;
        public const int TICKS_PER_SECONDS = 1000;
        public const int BIN_NORMAL_LEVEL_CONDITION = 100;
        public const int BIN_LOW_LEVEL_CONDITION = 200;
        public const int BIN_ZEROED_LEVEL_CONDITION = 300;
        public const int BIN_UNIT_DECREASING_NUMBER = 1;
        public const string BIN_DB_LOW_FLAG = "1";
        public const string BIN_DB_NORMAL_FLAG = "0";

        /// ========================================================================================================================================


        #endregion Constants

        #region Methods


        // STATION STARTER SETUP ===================================================================================================================

        /// <summary>
        /// Load Worker Categories into the ComboBox
        /// </summary>
        /// <param name="workerCategoryBox"></param>
        public static void LoadCategories(ComboBox workerCategoryBox)
        {
            // Locals
            var dtCategories = new DataTable();
            var bs = new BindingSource();

            try
            {
                // Call DAL functio to fill list with results of stored procedure
                var workerCategories = FLStationDAL.SelectWorkerCategory();

                // Bind list with comboBox
                bs.DataSource = workerCategories;
                workerCategoryBox.DataSource = bs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        /// ========================================================================================================================================


        // STATION SETUP ===========================================================================================================================

        /// <summary>
        /// Finds which should be the new stationID
        /// </summary>
        /// <returns></returns>
        public static int GetStationID()
        {
            // Locals
            var newStationID = 0;
            try
            {
                // Get StationID from Database
                newStationID = (FLStationDAL.SelectLastStationID() + 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.Message);
                throw ex;
            }

            return newStationID;
        }

        /// <summary>
        /// Sets up database tblStation for the instantiated Station
        /// </summary>
        /// <param name="workerID"></param>
        public static void SetupStationOnDB(int newStationID, int workerID)
        {
            try
            {
                // Insert collected data into the database; call DAL method
                FLStationDAL.InsertStationInfoOnDB(newStationID, workerID);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Get/Calculate Station speed based on timefactor and worker speed
        /// </summary>
        /// <param name="workerSpeed"></param>
        /// <param name="timeFactor"></param>
        /// <returns></returns>
        public static int GetStationInterval(int workerSpeed, int timeFactor)
        {
            //Locals
            var currentInterval = 0;

            try
            {
                // Gets Station speed factor based on configtable speed scale (return value in miliseconds)
                currentInterval = ((int)(workerSpeed / timeFactor))* 1000;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return currentInterval;
        }

        /// ========================================================================================================================================

        // STATION BIN SETUP =======================================================================================================================

        /// <summary>
        /// Returns a list of PartModel objs based on the Configuration table
        /// </summary>
        /// <returns></returns>
        public static List<PartModel> GetBinSetupFromConfigTable()
        {
            // Locals
            var binParts = new List<PartModel>();

            try
            {
                // Extract database config
                binParts = FLStationDAL.SelectBinPartsInitialConfig();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return binParts;
        }

        /// <summary>
        ///  Add Initial Bin information into database for a specific station
        /// </summary>
        /// <param name="stationID"></param>
        public static void SetupStationBinsOnDB(int stationID)
        {
            try
            {
                // Get Initial Configuration from ConfigTable
                var binParts = GetBinSetupFromConfigTable();

                // Insert data into tblBin
                //FLStationDAL.InsertStationBinsInfoOnDB(stationID, binParts);

                // Insert data into tblBinLine
                FLStationDAL.InsertStationBinLinesInfoOnDB(stationID, binParts);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// ========================================================================================================================================

        // STATION TEST TRAY SETUP =================================================================================================================

        /// <summary>
        /// Generates a new TestTrayID based on the last record in the database
        /// </summary>
        /// <returns></returns>
        public static string GenerateTestTrayID()
        {
            // Locals
            var lastTrayID = "";
            var numericalTrayID = 0;
            var newTrayID = new StringBuilder();

            try
            {
                // Get last trayID from database
                lastTrayID = FLStationDAL.SelectTestTrayID();

                // Extract numerical portion of TrayID
                Match match = Regex.Match(lastTrayID, @"(\d+)");
                if (match.Success)
                {
                    numericalTrayID = int.Parse(match.Groups[1].Value);
                }

                // Builds the new TrayID
                newTrayID.AppendFormat("FL{0,6:D6}", (numericalTrayID + 1));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


            return newTrayID.ToString();
        }

        /// <summary>
        /// Add a new TestTray in the Database
        /// </summary>
        /// <param name="trayID"></param>
        /// <param name="stationID"></param>
        public static void AddNewTestTrayToDB(string trayID, int stationID)
        {
            try
            {
                FLStationDAL.InsertTestTrayInfo(trayID, stationID);

                FLStationDAL.InsertTestTrayLineInfo(trayID, stationID);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        /// <summary>
        /// Add TestTray information into the database for a specific station
        /// </summary>
        /// <param name="theStationID"></param>
        public static void SetupStationTestTrayInfoOnDB(int theStationID, string newTestTrayID)
        {
            try
            {
                // Setup the tblTestTray with initial station data
                FLStationDAL.InsertTestTrayInfo(newTestTrayID,theStationID);

                // Setup the tblTestTrayLine with initial station data
                FLStationDAL.InsertTestTrayLineInfo(newTestTrayID, theStationID);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// ========================================================================================================================================


        // PRODUCTION INTERVAL CHECKERS ============================================================================================================

        /// <summary>
        /// Get time factor to adjust production time
        /// </summary>
        /// <returns></returns>
        public static int GetTimeFactor()
        {
            var currentTimeFactor = 0;

            try
            {
                currentTimeFactor = FLStationDAL.SelectTimeFactor();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.Message);
            }

            return currentTimeFactor;
        }
        
        /// <summary>
        /// Decrease the number of parts inside a bin by a pre-stablished number of units
        /// </summary>
        /// <param name="stationID"></param>
        /// <param name="binList"></param>
        /// <param name="numOfPartsToDecrease"></param>
        public static void DecreaseBinParts(int stationID, List<PartModel> binList, int numOfPartsToDecrease)
        {
            try
            {
                for (int i = 0; i < binList.Count; i++)
                {
                    FLStationDAL.UpdateBinParts(stationID, (i+1), numOfPartsToDecrease);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        
        /// <summary>
        /// If number of parts
        /// </summary>
        /// <param name="stationID"></param>
        /// <param name="partID"></param>
        /// <param name="partMinLevel"></param>
        /// <param name="stationBinDisplay"></param>
        public static int VerifyBinLineStockFlag(int stationID, int partID, int partMinLevel, TextBox stationBinDisplay)
        {
            // Locals
            int currentNumOfParts = 0;
            int displayIndicator = 0;
            int.TryParse(stationBinDisplay.Text, out currentNumOfParts);

            if ((currentNumOfParts < partMinLevel) && (currentNumOfParts > 0))
            {
                // Call DAL method to change bin flag 
                FLStationDAL.UpdateBinStockFlag(stationID, partID, BIN_DB_LOW_FLAG);

                // set indicator to change bin status color
                displayIndicator = BIN_LOW_LEVEL_CONDITION;
            }
            else if (currentNumOfParts <= 0)
            {
                // set indicator to change bin status color
                displayIndicator = BIN_ZEROED_LEVEL_CONDITION;
            }
            else 
            {
                // set indicator to change bin status color
                displayIndicator = BIN_NORMAL_LEVEL_CONDITION;
            }

            return displayIndicator;
        }

        /// <summary>
        /// Change Station Display textboxes according to database values
        /// </summary>
        /// <param name="stationID"></param>
        /// <param name="binList"></param>
        /// <param name="binsTextBoxes"></param>
        public static void ChangeBinsDisplayNumbers(int stationID, List<PartModel> binList,  List<TextBox> binsTextBoxes)
        {
            try
            {
                for (int i = 0; i < binsTextBoxes.Count; i++)
                {
                    var textBoxUpdatedNumber = FLStationDAL.SelectBinCurrentNumberOfParts(stationID, binList[i].PartID);
                    binsTextBoxes[i].Text = textBoxUpdatedNumber.ToString();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Set new Fog Lamp ID for a determined TestTray
        /// </summary>
        /// <param name="testTrayID"></param>
        /// <returns></returns>
        public static string CreateNewFogLampID(string testTrayID, int lampCounter)
        {
            StringBuilder lampID = new StringBuilder();

            try
            {
                lampID.AppendFormat("{0}{1:00}", testTrayID, lampCounter);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return lampID.ToString();
        }

        /// <summary>
        /// Adds a new Fog lamp to the test tray in the database
        /// </summary>
        /// <param name="testTrayID"></param>
        /// <param name="fogLampID"></param>
        public static void AddNewLampToTestTray(string testTrayID, string fogLampID, string testResult, int trayLampCount)
        {
            try
            {
                // Add new Lamp to the tbFogLamp
                FLStationDAL.InsertNewLampIDIntoDB(testTrayID, fogLampID, testResult);

                // Increment TestTray Lamp Count
                FLStationDAL.UpdateLampCountIntoTestTray(testTrayID, trayLampCount);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        /// ==========================================================================================================================================

        #endregion Methods
    }
}
