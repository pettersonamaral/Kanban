﻿/*****************************************************************************************************
* FILE : 		  FLStationDAL.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-16


* DESCRIPTION/Requirements: 
    Data Access Layer, access the database based on requests of the Business Layer
    
* Refence/sources:
*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FLStation;
using SQL_FinalProject;


namespace FLStation
{

    /// <summary>
    /// Gets the parameters from Business Logic Layer and uses to query the database
    /// </summary>
    public static class FLStationDAL
    {
        #region Properties

        // Properties
        static SqlConnection conn;
        private static Database theDatabase = Database.Instance;

        #endregion Properties

        #region Methods

        // DATABASE CONNECTION SETUP ===============================================================================================================

        /// <summary>
        /// Setup database connection based on AppConfig connection String
        /// </summary>
        /// <returns></returns>
        public static SqlConnection SetDBConnection()
        {
            // Setup Connection with database
           conn = new SqlConnection();
            try
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["SQL_FinalProject_Kanban"].ConnectionString;
            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Could not connect with Database. Invalid Connection String \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            return conn;
        }

        /// ========================================================================================================================================


        // STATION STARTER SETUP ===================================================================================================================

        /// <summary>
        /// Use a Storage Procedure to get all the available categories from the database
        /// </summary>
        /// <returns></returns>
        public static List<String> SelectWorkerCategory()
        {
            // Locals
            var allCategories = new List<String>();
            var conn = SetDBConnection();
            var cmd = new SqlCommand();
            SqlDataReader rdr = null;

            try
            {
                // Setup COmmand
                cmd.Connection = conn;
                cmd.CommandText = "getAllWorkerCategories";
                cmd.CommandType = CommandType.StoredProcedure;

                // Open Connection
                conn.Open();

                // Setup Reader
                rdr = cmd.ExecuteReader();

                // Loop through information and add to the list
                while (rdr.Read())
                {
                    //[STOPPED HERE] 0:28 min from demo
                    allCategories.Add(rdr[0].ToString());
                }
            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Couldn't retrieve the Categories. Invalid Connection String \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            finally
            {
                // Close Reader
                if (rdr != null && !rdr.IsClosed)
                {
                    rdr.Close();
                }

                // Close connection
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }

            }

            return allCategories;
        }

        /// ========================================================================================================================================


        // STATION SETUP ===========================================================================================================================


        /// <summary>
        /// Gets the Database Config Table Current Time Factor to adjust production interval
        /// </summary>
        /// <returns></returns>
        public static int SelectTimeFactor()
        {
            // Locals
            var currentTimeFactor = 0;
            var conn = SetDBConnection();
            var cmd = new SqlCommand();
            SqlDataReader rdr = null;

            try
            {
                // Setup COmmand
                cmd.Connection = conn;
                cmd.CommandText = "getTimeFactor";
                cmd.CommandType = CommandType.StoredProcedure;

                // Open Connection
                conn.Open();

                // Setup Reader
                rdr = cmd.ExecuteReader();

                // Loop through information and add to the list
                while (rdr.Read()) { int.TryParse((rdr[0].ToString()), out currentTimeFactor); }

            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Couldn't retrieve the Time Scale Factor \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            finally
            {
                // Close Reader
                if (rdr != null && !rdr.IsClosed) { rdr.Close(); }

                // Close connection
                if (conn.State != ConnectionState.Closed) { conn.Close(); }
            }

            return currentTimeFactor;
        }

        /// <summary>
        /// Check the ID of the last Station created
        /// </summary>
        /// <returns></returns>
        public static int SelectLastStationID()
        {
            // Locals
            var currentStationID = 0;
            var conn = SetDBConnection();
            var cmd = new SqlCommand();
            SqlDataReader rdr = null;

            try
            {
                // Setup COmmand
                cmd.Connection = conn;
                cmd.CommandText = "checkLastStationID";
                cmd.CommandType = CommandType.StoredProcedure;

                // Open Connection
                conn.Open();

                // Setup Reader
                rdr = cmd.ExecuteReader();

                // Get StationID number count
                //currentStationID = rdr.HasRows ? rdr.H : 0;
                while (rdr.Read())
                {
                    currentStationID++;
                }

            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Couldn't check StationID last number \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            finally
            {
                // Close Reader
                if (rdr != null && !rdr.IsClosed) { rdr.Close(); }

                // Close connection
                if (conn.State != ConnectionState.Closed) { conn.Close(); }
            }

            return currentStationID;
        }

        /// <summary>
        /// Insert Station information on Database everytime a station is instatiated
        /// </summary>
        /// <param name="newStationID"></param>
        /// <param name="selectedWorkerID"></param>
        public static void InsertStationInfoOnDB(int newStationID, int selectedWorkerID)
        {
            // Locals
            var cmd = new SqlCommand();
            var conn = SetDBConnection();

            try
            {
                // Setup COmmand
                cmd.Connection = conn;
                cmd.CommandText = "addtblStationInfo";
                cmd.CommandType = CommandType.StoredProcedure;

                // Add parameters
                cmd.Parameters.Add("@newStationID", SqlDbType.Int).Value = newStationID;
                cmd.Parameters.Add("@selectedWorkerID", SqlDbType.Int).Value = selectedWorkerID;

                // Open Connection
                conn.Open();

                // Execute
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Couldn't add new Test Tray Data into the database \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            finally
            {
                // Close connection
                if (conn.State != ConnectionState.Closed) { conn.Close(); }
            }
        }

        /// ========================================================================================================================================


        // STATION BIN SETUP =======================================================================================================================

        /// <summary>
        /// Get Database Bin initial configuration from Config table
        /// </summary>
        /// <returns></returns>
        public static List<PartModel> SelectBinPartsInitialConfig()
        {
            // Locals
            var binParts = new List<PartModel>();
            var conn = SetDBConnection();
            var cmd = new SqlCommand();
            SqlDataReader rdr = null;

            try
            {
                // Setup COmmand
                cmd.Connection = conn;
                cmd.CommandText = "getInitialBinPartsConfig";
                cmd.CommandType = CommandType.StoredProcedure;

                // Open Connection
                conn.Open();

                // Setup Reader
                rdr = cmd.ExecuteReader();

                // Loop through information and add to the list
                while (rdr.Read())
                {
                    var thePart = new PartModel();

                    // Set PartID
                    int.TryParse(rdr[0].ToString(), out var thePartID);
                    thePart.PartID = thePartID;

                    // Set PartName
                    thePart.PartName = rdr[1].ToString();

                    // Set BinMax
                    int.TryParse(rdr[2].ToString(), out var thePartBinMax);
                    thePart.MaxBinQty = thePartBinMax;

                    // Set BinMin
                    int.TryParse(rdr[3].ToString(), out var thePartBinMin);
                    thePart.MinBinQty = thePartBinMin;

                    // Add to List
                    binParts.Add(thePart);
                }
            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Couldn't retrieve the Bin Parts Configuration \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            finally
            {
                // Close Reader
                if (rdr != null && !rdr.IsClosed) { rdr.Close(); }

                // Close connection
                if (conn.State != ConnectionState.Closed) { conn.Close(); }
            }

            return binParts;
        }


        /// <summary>
        /// Insert BinLine information on tblBinLine everytime a station is instatiated
        /// </summary>
        /// <param name="stationID"></param>
        /// <param name="binPartsInitialConfig"></param>
        public static void InsertStationBinLinesInfoOnDB(int stationID, List<PartModel> binPartsInitialConfig)
        {
            // Locals
            var binList = new List<BinModel>();
            var conn = SetDBConnection();

            try
            {
                // Set binList considering current parts configuration
                for (var i = 0; i < binPartsInitialConfig.Count(); i++)
                {
                    var stationBin = new BinModel();
                    
                    stationBin.BinID = (i + 1);
                    stationBin.StationID = stationID;
                    stationBin.StockFlag = BinModel.StockFlagOK;
                    stationBin.CurrentQty = binPartsInitialConfig[i].MaxBinQty;

                    binList.Add(stationBin);
                }

                foreach (var item in binList)
                {
                    // Setup COmmand
                    var cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = "addBinLineInfo";
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Open Connection
                    conn.Open();
                    cmd.Parameters.Add("@stationID", SqlDbType.Int).Value = item.StationID;
                    cmd.Parameters.Add("@binID", SqlDbType.Int).Value = item.BinID;
                    cmd.Parameters.Add("@stockFlag", SqlDbType.NVarChar,3).Value = item.StockFlag;
                    cmd.Parameters.Add("@currentQty", SqlDbType.Int).Value = item.CurrentQty;

                    // Execute
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Couldn't add Station BinLine info to the database \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            finally
            {
                // Close connection
                if (conn.State != ConnectionState.Closed) { conn.Close(); }
            }
        }


        /// <summary>
        /// Insert Bin information on tblBin everytime a station is instantiated
        /// </summary>
        /// <param name="stationID"></param>
        /// <param name="binPartsInitialConfig"></param>
        public static void InsertStationBinsInfoOnDB(int stationID, List<PartModel> binPartsInitialConfig)
        {
            // Locals
            var binList = new List<BinModel>();
            var conn = SetDBConnection();

            try
            {
                // Set binList considering current parts configuration
                for (var i = 0; i < binPartsInitialConfig.Count(); i++)
                {
                    var stationBin = new BinModel();
                    binList.Add(stationBin);

                    stationBin.BinID = (i + 1);
                    stationBin.StationID = stationID;
                    stationBin.PartID = binPartsInitialConfig[i].PartID;
                    stationBin.StockFlag = BinModel.StockFlagOK;
                    stationBin.CurrentQty = binPartsInitialConfig[i].MaxBinQty;
                }

                foreach (var binModel in binList)
                {

                    // Setup COmmand
                    var cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = "addBinInfo";
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Open Connection
                    conn.Open();

                    cmd.Parameters.Add("@binID", SqlDbType.Int).Value = binModel.BinID;
                    cmd.Parameters.Add("@partID", SqlDbType.Int).Value = binModel.PartID;

                    // Execute
                    cmd.ExecuteNonQuery();

                    // Clear parameters
                    cmd.Parameters.Clear();

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Couldn't add station bins info to the database \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            finally
            {
                // Close connection
                if (conn.State != ConnectionState.Closed) { conn.Close(); }
            }


        }

        /// ========================================================================================================================================


        // STATION TEST TRAY SETUP =================================================================================================================

        /// <summary>
        /// Get the last TestTrayID in the Database
        /// </summary>
        /// <returns></returns>
        public static string SelectTestTrayID()
        {
            // Locals
            var lastTrayID = "";
            var conn = SetDBConnection();
            var cmd = new SqlCommand();
            SqlDataReader rdr = null;

            try
            {
                // Setup COmmand
                cmd.Connection = conn;
                cmd.CommandText = "getLastTestTrayID";
                cmd.CommandType = CommandType.StoredProcedure;

                // Open Connection
                conn.Open();

                // Setup Reader
                rdr = cmd.ExecuteReader();

                // Get StationID number count
                if (rdr.Read()) { lastTrayID = rdr.GetString(0); }
            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Couldn't get last TestTrayID value \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            finally
            {
                // Close Reader
                if (rdr != null && !rdr.IsClosed) { rdr.Close(); }

                // Close connection
                if (conn.State != ConnectionState.Closed) { conn.Close(); }
            }

            return lastTrayID;
        }

        /// <summary>
        /// Gets the Database Config Table Current Time Factor to adjust production interval
        /// </summary>
        /// <returns></returns>
        public static void InsertTestTrayInfo(string testTrayID, int stationID)
        {
            // Locals
            //var currentTimeFactor = 0;
            var conn = SetDBConnection();
            var cmd = new SqlCommand();

            try
            {
                // Setup COmmand
                cmd.Connection = conn;
                cmd.CommandText = "addTestTrayInfo";
                cmd.CommandType = CommandType.StoredProcedure;

                // Add parameters
                cmd.Parameters.Add("@newTestTrayID", SqlDbType.NVarChar, 8).Value = testTrayID;
                cmd.Parameters.Add("@stationID", SqlDbType.Int).Value = stationID;

                // Open Connection
                conn.Open();

                // Execute
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Couldn't add new Test Tray Data into the database \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            finally
            {
                // Close connection
                if (conn.State != ConnectionState.Closed) { conn.Close(); }
            }
        }

        /// <summary>
        /// Gets the Database Config Table Current Time Factor to adjust production interval
        /// </summary>
        /// <returns></returns>
        public static void InsertTestTrayLineInfo(string testTrayID, int stationID)
        {
            // Locals
            var conn = SetDBConnection();
            var cmd = new SqlCommand();

            try
            {
                // Setup COmmand
                cmd.Connection = conn;
                cmd.CommandText = "addTestTrayLineInfo";
                cmd.CommandType = CommandType.StoredProcedure;

                // Add parameters
                cmd.Parameters.Add("@newTestTrayID", SqlDbType.NVarChar, 8).Value = testTrayID;
                cmd.Parameters.Add("@stationID", SqlDbType.Int).Value = stationID;

                // Open Connection
                conn.Open();
                
                // Execute
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Couldn't add new Test Tray Data into the database \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            finally
            {
                // Close connection
                if (conn.State != ConnectionState.Closed) { conn.Close(); }
            }
        }

        /// ========================================================================================================================================


        // STATION PRODUCTION INTERVAL CHECKERS ====================================================================================================

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedPartID"></param>
        /// <returns></returns>
        public static int SelectBinCurrentNumberOfParts(int stationID, int binPartID)
        {
            // Locals
            var currentPartUnits = 0;
            var conn = SetDBConnection();
            var cmd = new SqlCommand();
            SqlDataReader rdr = null;

            try
            {
                // Setup COmmand
                cmd.Connection = conn;
                cmd.CommandText = "checkBinPartsCount";
                cmd.CommandType = CommandType.StoredProcedure;

                // Add parameters
                cmd.Parameters.Add("@stationID", SqlDbType.Int).Value = stationID;
                cmd.Parameters.Add("@binPartID", SqlDbType.Int).Value = binPartID;

                // Open Connection
                conn.Open();

                // Setup Reader
                rdr = cmd.ExecuteReader();

                // Loop through information and add to the list
                while (rdr.Read()) { int.TryParse((rdr[0].ToString()), out currentPartUnits); }

            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Couldn't retrieve the Bin current number of parts \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            finally
            {
                // Close Reader
                if (rdr != null && !rdr.IsClosed) { rdr.Close(); }

                // Close connection
                if (conn.State != ConnectionState.Closed) { conn.Close(); }
            }

            return currentPartUnits;
        }

        /// <summary>
        /// Add new produced Fog Lamp to the Database
        /// </summary>
        /// <param name="testTrayID"></param>
        /// <param name="fogLampID"></param>
        public static void InsertNewLampIDIntoDB(string testTrayID, string fogLampID, string testResult)
        {
            // Locals
            var conn = SetDBConnection();
            var cmd = new SqlCommand();

            try
            {
                // Setup COmmand
                cmd.Connection = conn;
                cmd.CommandText = "addNewFogLampID";
                cmd.CommandType = CommandType.StoredProcedure;

                // Add parameters
                cmd.Parameters.Add("@testTrayID", SqlDbType.NVarChar, 8).Value = testTrayID;
                cmd.Parameters.Add("@newFogLampID", SqlDbType.NVarChar, 10).Value = fogLampID;
                cmd.Parameters.Add("@timeStamp", SqlDbType.DateTime).Value = DateTime.Now.ToShortTimeString();
                cmd.Parameters.Add("@passTest", SqlDbType.NVarChar, 3).Value = testResult; 

                // Open Connection
                conn.Open();

                // Execute
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Couldn't add new Fog Lamp Data into the database \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            finally
            {
                // Close connection
                if (conn.State != ConnectionState.Closed) { conn.Close(); }
            }
        }

        /// <summary>
        /// Update the number of parts from a bin whenever a lamp is produced
        /// </summary>
        /// <param name="stationID"></param>
        /// <param name="binID"></param>
        /// <param name="numOfPartsToDecrease"></param>
        public static void UpdateBinParts(int stationID, int binID, int numOfPartsToDecrease )
        {
            // Locals
            var conn = SetDBConnection();
            var cmd = new SqlCommand();

            try
            {
                // Setup COmmand
                cmd.Connection = conn;
                cmd.CommandText = "subtractBinPart";
                cmd.CommandType = CommandType.StoredProcedure;

                // Add parameters
                cmd.Parameters.Add("@stationID", SqlDbType.Int).Value = stationID;
                cmd.Parameters.Add("@binID", SqlDbType.Int).Value = binID;
                cmd.Parameters.Add("@decreaseNumber", SqlDbType.Int).Value = numOfPartsToDecrease;

                // Open Connection
                conn.Open();

                // Execute
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Couldn't update the part number into the database \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            finally
            {
                // Close connection
                if (conn.State != ConnectionState.Closed) { conn.Close(); }
            }
        }

        /// <summary>
        /// Change the BinStockFlag inside the tblBin to activate runner action
        /// </summary>
        /// <param name="lowFlag"></param>
        public static void UpdateBinStockFlag(int stationID, int binPartID, string lowFlag)
        {
            // Locals
            var conn = SetDBConnection();
            var cmd = new SqlCommand();

            try
            {
                // Setup COmmand
                cmd.Connection = conn;
                cmd.CommandText = "changeBinStockFlag";
                cmd.CommandType = CommandType.StoredProcedure;

                // Add parameters
                cmd.Parameters.Add("@stationID", SqlDbType.Int).Value = stationID;
                cmd.Parameters.Add("@binPartID", SqlDbType.Int).Value = binPartID;
                cmd.Parameters.Add("@flagValue", SqlDbType.Int).Value = lowFlag;

                // Open Connection
                conn.Open();

                // Execute
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Couldn't change Bin Stock Flag into the database \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            finally
            {
                // Close connection
                if (conn.State != ConnectionState.Closed) { conn.Close(); }
            }
        }

        public static void UpdateLampCountIntoTestTray(string testTrayID, int trayLampCount)
        {
            // Locals
            var conn = SetDBConnection();
            var cmd = new SqlCommand();

            try
            {
                // Setup COmmand
                cmd.Connection = conn;
                cmd.CommandText = "addLampCounttoTestTray";
                cmd.CommandType = CommandType.StoredProcedure;

                // Add parameters
                cmd.Parameters.Add("@testTrayID", SqlDbType.NVarChar, 8).Value = testTrayID;
                cmd.Parameters.Add("@lampCount", SqlDbType.Int).Value = trayLampCount;

                // Open Connection
                conn.Open();

                // Execute
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                var fullException = new StringBuilder();
                fullException.AppendFormat(
                    "[FLSTATION] Couldn't increment lamp count on Test Tray \n[EXCEPTION] {0}", ex.Message);
                throw (new FLStationDALException(fullException.ToString()));
            }
            finally
            {
                // Close connection
                if (conn.State != ConnectionState.Closed) { conn.Close(); }
            }
        }


        /// ========================================================================================================================================

        #endregion Methods
    }    
}

