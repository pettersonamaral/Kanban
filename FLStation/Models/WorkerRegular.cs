﻿/*****************************************************************************************************
* FILE : 		  WorkerRegular.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-16


* DESCRIPTION/Requirements: 
    Models a regular station worker
    
* Refence/sources:
*****************************************************************************************************/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FLStation
{
    class WorkerRegular: Worker, IEmployee
    {
        #region Methods

        /// <summary>
        /// Get regular worker category
        /// </summary>
        /// <returns></returns>
        public string GetWorkerCategory()
        {
            Category = RegularWorkerCategory;
            return Category;
        }

        /// <summary>
        /// Get regular worker average speed
        /// </summary>
        /// <returns></returns>
        public float GetAvgWorkerSpeed()
        {
            AvgAssemblyTime = RegularAvgSpeedSec;
            return AvgAssemblyTime;
        }

        /// <summary>
        /// Get regular worker average error
        /// </summary>
        /// <returns></returns>
        public float GetAvgWorkerError()
        {
            AvgDeffectOcurrence = RegularErrorRate;
            return AvgDeffectOcurrence;
        }

        #endregion Methods
    }
}
