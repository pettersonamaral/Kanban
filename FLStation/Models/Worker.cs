﻿/*****************************************************************************************************
* FILE : 		  Worker.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-16


* DESCRIPTION/Requirements: 
    Parent worker class, with basic properties
    
* Refence/sources:
*****************************************************************************************************/
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FLStation
{
    abstract class Worker
    {
        #region Properties

        // Properties
        protected string  Category            { get; set; }
        protected float   AvgAssemblyTime     { get; set; }
        protected float   AvgDeffectOcurrence { get; set; }

        #endregion Properties

        #region Constants

        // Constants 

        // Workers Category
        public const String RegularWorkerCategory       = "Regular";
        public const String ExperiencedWorkerCategory   = "Experienced";
        public const String RookieWorkerCategory        = "Rookie";

        public const int RookieWorkerCategoryID           = 1;
        public const int RegularWorkerCategoryID          = 2;
        public const int ExperiencedWorkerCategoryID      = 3;
 

        // Workers Speed
        public const float RegularAvgSpeedSec          = 60;
        public const float ExperiencedAvgSpeedSec      = 51;
        public const float RookieAvgSpeedSec           = 90;

        // Worker Error Margin
        public const float RookieErrorRate             = 0.0085F;
        public const float RegularErrorRate            = 0.0050F;
        public const float ExperiencedErrorRate        = 0.0015F;
        

        #endregion Constants
    }
}
