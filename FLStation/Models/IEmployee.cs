﻿/*****************************************************************************************************
* FILE : 		  IEmployee.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-16


* DESCRIPTION/Requirements: 
    Simplistic interface implementation for Worker types..., probably not necessary at this level
    
* Refence/sources:
*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FLStation
{
    interface IEmployee
    {
        #region Methods

        // Methods
        string GetWorkerCategory ();
        float    GetAvgWorkerSpeed ();
        float    GetAvgWorkerError ();

        #endregion Methods
    }
}
