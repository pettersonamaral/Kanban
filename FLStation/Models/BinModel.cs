﻿/*****************************************************************************************************
* FILE : 		  BinModel.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-16


* DESCRIPTION/Requirements: 
    Initial Model of the Station Bin
    
* Refence/sources:
*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FLStation
{
    class BinModel
    {

        #region Properties

        public int       BinID         { get; set; }
        public int       StationID     { get; set; }
        public int       PartID        { get; set; }
        public string    StockFlag     { get; set; }
        public int       CurrentQty    { get; set; } 
        public PartModel BinPart       { get; set; }

        #endregion Properties

        #region Constants

        public const string StockFlagOK  = "0";  
        public const string StockFlagLow = "1";

        #endregion Constants

    }
}
