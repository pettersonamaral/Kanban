﻿/*****************************************************************************************************
* FILE : 		  PartModel.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-16


* DESCRIPTION/Requirements: 
    Basic model of the Fog Lamp parts 
    
* Refence/sources:
*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FLStation
{
    public class PartModel
    {
        #region Properties

        public int    PartID      { get; set; }         
        public string PartName    { get; set; }
        public int    MaxBinQty   { get; set; }
        public int    MinBinQty   { get; set; }

        #endregion Properties
    }
}
