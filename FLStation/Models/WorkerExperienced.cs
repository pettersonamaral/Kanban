﻿/*****************************************************************************************************
* FILE : 		  WorkerExperienced.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-16


* DESCRIPTION/Requirements: 
    Models an experienced station worker
    
* Refence/sources:
*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FLStation
{
    class WorkerExperienced: Worker, IEmployee
    {
        #region Methods

        /// <summary>
        /// Get Experienced worker category
        /// </summary>
        /// <returns></returns>
        public string GetWorkerCategory()
        {
            Category = ExperiencedWorkerCategory;
            return Category;
        }


        /// <summary>
        /// Get Experienced worker average speed
        /// </summary>
        /// <returns></returns>
        public float GetAvgWorkerSpeed()
        {
            AvgAssemblyTime = ExperiencedAvgSpeedSec;
            return AvgAssemblyTime;
        }


        /// <summary>
        /// Get Experienced worker work error
        /// </summary>
        /// <returns></returns>
        public float GetAvgWorkerError()
        {
            AvgDeffectOcurrence = ExperiencedErrorRate;
            return AvgDeffectOcurrence;
        }

        #endregion Methods
    }
}
