﻿/*****************************************************************************************************
* FILE : 		  WorkerRookie.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-16


* DESCRIPTION/Requirements: 
    Models a rookie station worker
    
* Refence/sources:
*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FLStation
{
    class WorkerRookie: Worker , IEmployee
    {
        #region Methods

        /// <summary>
        /// Get rookie worker category
        /// </summary>
        /// <returns></returns>
        public string GetWorkerCategory()
        {
            Category = RookieWorkerCategory;
            return Category;
        }

        /// <summary>
        /// Get rookie worker average speed
        /// </summary>
        /// <returns></returns>
        public float GetAvgWorkerSpeed()
        {
            AvgAssemblyTime = RookieAvgSpeedSec;
            return AvgAssemblyTime;
        }
        
        /// <summary>
        /// Get rookie worker average error
        /// </summary>
        /// <returns></returns>
        public float GetAvgWorkerError()
        {
            AvgDeffectOcurrence = RookieErrorRate;
            return AvgDeffectOcurrence;
        }

        #endregion Methods
    }
}

