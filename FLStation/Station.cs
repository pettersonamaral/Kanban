﻿/*****************************************************************************************************
* FILE : 		  Station.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-16


* DESCRIPTION/Requirements: 
    Fog Lamp Station Form representing a workerstation
    
* Refence/sources:
*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using FLStation;

namespace FLStation
{
    public partial class Station : Form
    {
        #region Constants

        private const int LAMP_UNIT = 1;

        #endregion

        #region Local Attributes

        // Locals
        private List<PartModel>  binPartsSetup;
        private int              theStationID;
        private int              totalLampTracker;
        private int              testTrayLampTracker;
        private int              timeFactor;
        private double           errorFactor;
        private string           testResult;
        private string           theTestTrayID;
        private List<TextBox>    binsNumbersDisplay;
        private List<Panel>      binsColorsDisplay;

        #endregion Local Attributes

        #region Form Methods

        public Station()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Alternate Constructor
        /// </summary>
        /// <param name="workerID"></param>
        public Station(int workerID)
        {
            InitializeComponent();

            // SET WORKER ATTRIBUTES ===============================================================================================================

            if (workerID == Worker.ExperiencedWorkerCategoryID)
            {
                var theWorker = new WorkerExperienced();
                tbWorkerExperience.Text = theWorker.GetWorkerCategory();
                tbErrorMargin.Text = theWorker.GetAvgWorkerError().ToString();
                tbWorkerSpeed.Text = theWorker.GetAvgWorkerSpeed().ToString();
            }
            else if (workerID == Worker.RegularWorkerCategoryID)
            {
                var theWorker = new WorkerRegular();
                tbWorkerExperience.Text = theWorker.GetWorkerCategory();
                tbErrorMargin.Text = theWorker.GetAvgWorkerError().ToString();
                tbWorkerSpeed.Text = theWorker.GetAvgWorkerSpeed().ToString();
            }
            else if (workerID == Worker.RookieWorkerCategoryID)
            {
                var theWorker = new WorkerRookie();
                tbWorkerExperience.Text = theWorker.GetWorkerCategory();
                tbErrorMargin.Text = theWorker.GetAvgWorkerError().ToString();
                tbWorkerSpeed.Text = theWorker.GetAvgWorkerSpeed().ToString();
            }

            // SET STATION TIMER ATTRIBUTES ========================================================================================================

            // Check  Work Speed and Time Scale and define time tracker event interval
            int.TryParse(tbWorkerSpeed.Text, out var workerSpeed);
            timeFactor = StationManager.GetTimeFactor();
            tbTimeCorrection.Text = timeFactor.ToString();
            float.TryParse(tbErrorMargin.Text, out var errorFactor);

            // Set StationTimer Interval (Based on what is currently on the config table)
            StationTimer.Interval = StationManager.GetStationInterval(workerSpeed, timeFactor);

            // SET STATION ATTRIBUTES ===============================================================================================================

            theStationID = StationManager.GetStationID();
            StationManager.SetupStationOnDB(theStationID, workerID);

            // SET STATION BIN ATTRIBUTES ===========================================================================================================

            StationManager.SetupStationBinsOnDB(theStationID);
            binPartsSetup = StationManager.GetBinSetupFromConfigTable();

            // Set List of Station Bin Number Displays
            binsNumbersDisplay = new List<TextBox> { tbHarness, tbReflector, tbHousing, tbLens, tbBulb, tbBezel };

            // Set List of Station Bin Color Displays
            binsColorsDisplay = new List<Panel> { plHarnessColor, plReflectorColor, plHousingColor, plLensColor, plBulbColor, plBezelColor };

            // SET STATION TEST TRAY ATTRIBUTES =====================================================================================================

            theTestTrayID = StationManager.GenerateTestTrayID();
            StationManager.SetupStationTestTrayInfoOnDB(theStationID, theTestTrayID);
            tbTesttrayID.Text = theTestTrayID;

            // SET STATION DISPLAY ATTRIBUTES =======================================================================================================

            LoadBinTextBoxes();
            tbStartTime.Text = DateTime.Now.ToShortTimeString();
            tbStationID.Text = theStationID.ToString();
            tbNumFogLamps.Text = testTrayLampTracker.ToString();

            // START THE TIMER ======================================================================================================================

            StationTimer.Start();
        }

        /// <summary>
        /// Load Initial Bin configurations
        /// </summary>
        private void LoadBinTextBoxes()
        {
            var binConfigList = StationManager.GetBinSetupFromConfigTable();

            // Set Initial TextBoxes Values
            tbHarness.Text = binConfigList[0].MaxBinQty.ToString();
            tbReflector.Text = binConfigList[1].MaxBinQty.ToString();
            tbHousing.Text = binConfigList[2].MaxBinQty.ToString();
            tbLens.Text = binConfigList[3].MaxBinQty.ToString();
            tbBulb.Text = binConfigList[4].MaxBinQty.ToString();
            tbBezel.Text = binConfigList[5].MaxBinQty.ToString();

            // Set Panels initial colors
            plHarnessColor.BackColor = Color.GreenYellow;
            plReflectorColor.BackColor = Color.GreenYellow;
            plHousingColor.BackColor = Color.GreenYellow;
            plLensColor.BackColor = Color.GreenYellow;
            plBulbColor.BackColor = Color.GreenYellow;
            plBezelColor.BackColor = Color.GreenYellow;

        }

        /// <summary>
        /// Trigger a set of events to keep station up to date every time a lamp is produced
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StationTimer_Tick(object sender, EventArgs e)
        {
            // Embbed ERROR factor in the lamp production
            errorFactor = (float)Convert.ToDouble(tbErrorMargin.Text.ToString());
            if (totalLampTracker == 0) { totalLampTracker = 1; }
            if ((totalLampTracker * errorFactor) % LAMP_UNIT == 0) { testResult = "BAD"; }
            else { testResult = "OK"; }

            // Produce a lamp
            if (testTrayLampTracker == 0) { testTrayLampTracker = 1; }

            // Check how many lamps in the Test Tray already
            if (testTrayLampTracker == StationManager.TRAY_CAPACITY)
            {
                // Get another TestTray
                theTestTrayID = StationManager.GenerateTestTrayID();
                tbTesttrayID.Text = theTestTrayID;

                // Add new tray to the database
                StationManager.AddNewTestTrayToDB(theTestTrayID, theStationID);

                // Reset the lampTracker
                testTrayLampTracker = 0;

                // Add a new lamp to the database
                var newFogLampID = StationManager.CreateNewFogLampID(theTestTrayID, testTrayLampTracker);
                StationManager.AddNewLampToTestTray(theTestTrayID, newFogLampID, testResult, testTrayLampTracker);

                // Decrease Bin part units (Station panel and database) and update Bin Flag if necessary
                StationManager.DecreaseBinParts(theStationID, binPartsSetup, StationManager.BIN_UNIT_DECREASING_NUMBER);

                // Change Bin Display According to Database numbers
                StationManager.ChangeBinsDisplayNumbers(theStationID, binPartsSetup, binsNumbersDisplay);

                // Change Bin Flag if necessary
                CheckBinStockFlags(theStationID, binPartsSetup);
            }
            else
            {
                // Add lamp to the database
                var newFogLampID = StationManager.CreateNewFogLampID(theTestTrayID, testTrayLampTracker);
                StationManager.AddNewLampToTestTray(theTestTrayID, newFogLampID, testResult, testTrayLampTracker);

                // Increase the lamp counter by 1
                testTrayLampTracker++; // at the test tray
                totalLampTracker++;    // at the total count

                // Decrease Bin part units (Station panel and database) and update Bin Flag if necessary
                StationManager.DecreaseBinParts(theStationID, binPartsSetup, StationManager.BIN_UNIT_DECREASING_NUMBER);

                // Change Bin Display According to Database numbers
                StationManager.ChangeBinsDisplayNumbers(theStationID, binPartsSetup, binsNumbersDisplay);

                // Change Bin Flag if necessary
                CheckBinStockFlags(theStationID, binPartsSetup);
            }

            // Check for Bin/Parts Configuration Changes
            binPartsSetup = StationManager.GetBinSetupFromConfigTable();

            // Check  Work Speed and Time Scale and define time tracker event interval
            timeFactor = StationManager.GetTimeFactor();
            tbTimeCorrection.Text = timeFactor.ToString();
            //StationTimer.Interval = StationManager.GetStationInterval(workerSpeed, timeFactor);

            // Update Fog Lamp count
            tbNumFogLamps.Text = testTrayLampTracker.ToString();
        }


        #endregion

        #region Support Methods

        /// <summary>
        ///  Check Bin Flags and change Station Display colors accordingly
        /// </summary>
        /// <param name="stationID"></param>
        /// <param name="binList"></param>
        private void CheckBinStockFlags(int stationID, List<PartModel> binList)
        {
            //Locals
            int binColorIndicator = 0;
            try
            {
                for (int i = 0; i < binList.Count; i++)
                {
                    binColorIndicator = StationManager.VerifyBinLineStockFlag(stationID, binList[i].PartID, binList[i].MinBinQty,binsNumbersDisplay[i]);
                    SetPanelColor(binColorIndicator,binsColorsDisplay[i]);

                    // Stops Station production whenever red flags arise
                    if(binColorIndicator == StationManager.BIN_ZEROED_LEVEL_CONDITION) {  StationTimer.Stop();}
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Changes the color of the panel
        /// </summary>
        /// <param name="indicator"></param>
        /// <param name="indicatorColor"></param>
        private void SetPanelColor(int colorIndicator, Panel binColorDisplay)
        {
            // Change the panel color based on entry values
            switch (colorIndicator)
            {
                case StationManager.BIN_NORMAL_LEVEL_CONDITION:

                    binColorDisplay.BackColor = Color.GreenYellow;
                    break;

                case StationManager.BIN_LOW_LEVEL_CONDITION:

                    binColorDisplay.BackColor = Color.Orange;
                    break;
                case StationManager.BIN_ZEROED_LEVEL_CONDITION:

                    binColorDisplay.BackColor = Color.Red;
                    break;
            }
        }

        #endregion Support Methods
    }
}
