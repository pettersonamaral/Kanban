﻿/*****************************************************************************************************
* FILE : 		  StationStarter.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-16


* DESCRIPTION/Requirements: 
    Starter form for selection of worker type
    
* Refence/sources:
*****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FLStation
{
    public partial class StationStarter : Form
    {
        public StationStarter()
        {
            InitializeComponent();

            StationManager.LoadCategories(cbxWCategories);

        }

        private void btnStartStation_Click(object sender, EventArgs e)
        {
            // Start new Station
            Station newStation = new Station((cbxWCategories.SelectedIndex + 1));
            newStation.Show();
        }
    }
}
