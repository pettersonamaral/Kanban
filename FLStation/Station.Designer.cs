﻿namespace FLStation
{
    partial class Station
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Station));
            this.gpBins = new System.Windows.Forms.GroupBox();
            this.plBezelColor = new System.Windows.Forms.Panel();
            this.plBulbColor = new System.Windows.Forms.Panel();
            this.plLensColor = new System.Windows.Forms.Panel();
            this.plHousingColor = new System.Windows.Forms.Panel();
            this.plReflectorColor = new System.Windows.Forms.Panel();
            this.plHarnessColor = new System.Windows.Forms.Panel();
            this.tbBezel = new System.Windows.Forms.TextBox();
            this.tbBulb = new System.Windows.Forms.TextBox();
            this.tbLens = new System.Windows.Forms.TextBox();
            this.tbHousing = new System.Windows.Forms.TextBox();
            this.tbReflector = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbHarness = new System.Windows.Forms.TextBox();
            this.gpTestTray = new System.Windows.Forms.GroupBox();
            this.tbTesttrayID = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbNumFogLamps = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.gpStationConfig = new System.Windows.Forms.GroupBox();
            this.tbWorkerSpeed = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbTimeCorrection = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbErrorMargin = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbStartTime = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbStationID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbWorkerExperience = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.StationTimer = new System.Windows.Forms.Timer(this.components);
            this.gpBins.SuspendLayout();
            this.gpTestTray.SuspendLayout();
            this.gpStationConfig.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpBins
            // 
            this.gpBins.Controls.Add(this.plBezelColor);
            this.gpBins.Controls.Add(this.plBulbColor);
            this.gpBins.Controls.Add(this.plLensColor);
            this.gpBins.Controls.Add(this.plHousingColor);
            this.gpBins.Controls.Add(this.plReflectorColor);
            this.gpBins.Controls.Add(this.plHarnessColor);
            this.gpBins.Controls.Add(this.tbBezel);
            this.gpBins.Controls.Add(this.tbBulb);
            this.gpBins.Controls.Add(this.tbLens);
            this.gpBins.Controls.Add(this.tbHousing);
            this.gpBins.Controls.Add(this.tbReflector);
            this.gpBins.Controls.Add(this.label6);
            this.gpBins.Controls.Add(this.label5);
            this.gpBins.Controls.Add(this.label4);
            this.gpBins.Controls.Add(this.label3);
            this.gpBins.Controls.Add(this.label2);
            this.gpBins.Controls.Add(this.label1);
            this.gpBins.Controls.Add(this.tbHarness);
            this.gpBins.Location = new System.Drawing.Point(9, 10);
            this.gpBins.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gpBins.Name = "gpBins";
            this.gpBins.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gpBins.Size = new System.Drawing.Size(568, 102);
            this.gpBins.TabIndex = 0;
            this.gpBins.TabStop = false;
            this.gpBins.Text = "Fog Lamp Assembly Bins";
            // 
            // plBezelColor
            // 
            this.plBezelColor.Location = new System.Drawing.Point(487, 25);
            this.plBezelColor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.plBezelColor.Name = "plBezelColor";
            this.plBezelColor.Size = new System.Drawing.Size(74, 20);
            this.plBezelColor.TabIndex = 13;
            // 
            // plBulbColor
            // 
            this.plBulbColor.Location = new System.Drawing.Point(392, 25);
            this.plBulbColor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.plBulbColor.Name = "plBulbColor";
            this.plBulbColor.Size = new System.Drawing.Size(74, 20);
            this.plBulbColor.TabIndex = 13;
            // 
            // plLensColor
            // 
            this.plLensColor.Location = new System.Drawing.Point(296, 25);
            this.plLensColor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.plLensColor.Name = "plLensColor";
            this.plLensColor.Size = new System.Drawing.Size(74, 20);
            this.plLensColor.TabIndex = 13;
            // 
            // plHousingColor
            // 
            this.plHousingColor.Location = new System.Drawing.Point(200, 25);
            this.plHousingColor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.plHousingColor.Name = "plHousingColor";
            this.plHousingColor.Size = new System.Drawing.Size(74, 20);
            this.plHousingColor.TabIndex = 13;
            // 
            // plReflectorColor
            // 
            this.plReflectorColor.Location = new System.Drawing.Point(104, 25);
            this.plReflectorColor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.plReflectorColor.Name = "plReflectorColor";
            this.plReflectorColor.Size = new System.Drawing.Size(74, 20);
            this.plReflectorColor.TabIndex = 13;
            // 
            // plHarnessColor
            // 
            this.plHarnessColor.Location = new System.Drawing.Point(8, 25);
            this.plHarnessColor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.plHarnessColor.Name = "plHarnessColor";
            this.plHarnessColor.Size = new System.Drawing.Size(74, 20);
            this.plHarnessColor.TabIndex = 11;
            // 
            // tbBezel
            // 
            this.tbBezel.Location = new System.Drawing.Point(487, 64);
            this.tbBezel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbBezel.Name = "tbBezel";
            this.tbBezel.Size = new System.Drawing.Size(76, 20);
            this.tbBezel.TabIndex = 10;
            this.tbBezel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbBulb
            // 
            this.tbBulb.Location = new System.Drawing.Point(391, 64);
            this.tbBulb.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbBulb.Name = "tbBulb";
            this.tbBulb.Size = new System.Drawing.Size(76, 20);
            this.tbBulb.TabIndex = 9;
            this.tbBulb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbLens
            // 
            this.tbLens.Location = new System.Drawing.Point(295, 64);
            this.tbLens.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbLens.Name = "tbLens";
            this.tbLens.Size = new System.Drawing.Size(76, 20);
            this.tbLens.TabIndex = 8;
            this.tbLens.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbHousing
            // 
            this.tbHousing.Location = new System.Drawing.Point(199, 64);
            this.tbHousing.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbHousing.Name = "tbHousing";
            this.tbHousing.Size = new System.Drawing.Size(76, 20);
            this.tbHousing.TabIndex = 7;
            this.tbHousing.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbReflector
            // 
            this.tbReflector.Location = new System.Drawing.Point(103, 64);
            this.tbReflector.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbReflector.Name = "tbReflector";
            this.tbReflector.Size = new System.Drawing.Size(76, 20);
            this.tbReflector.TabIndex = 1;
            this.tbReflector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(508, 48);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Bezel";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(415, 48);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Bulb";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(318, 48);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Lens";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(214, 48);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Housing";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(116, 48);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Reflector";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Harness";
            // 
            // tbHarness
            // 
            this.tbHarness.Location = new System.Drawing.Point(7, 64);
            this.tbHarness.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbHarness.Name = "tbHarness";
            this.tbHarness.Size = new System.Drawing.Size(76, 20);
            this.tbHarness.TabIndex = 0;
            this.tbHarness.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // gpTestTray
            // 
            this.gpTestTray.Controls.Add(this.tbTesttrayID);
            this.gpTestTray.Controls.Add(this.label15);
            this.gpTestTray.Controls.Add(this.tbNumFogLamps);
            this.gpTestTray.Controls.Add(this.label12);
            this.gpTestTray.Location = new System.Drawing.Point(435, 117);
            this.gpTestTray.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gpTestTray.Name = "gpTestTray";
            this.gpTestTray.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gpTestTray.Size = new System.Drawing.Size(142, 119);
            this.gpTestTray.TabIndex = 2;
            this.gpTestTray.TabStop = false;
            this.gpTestTray.Text = "Fog Lamp Test Tray";
            // 
            // tbTesttrayID
            // 
            this.tbTesttrayID.Location = new System.Drawing.Point(16, 39);
            this.tbTesttrayID.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbTesttrayID.Name = "tbTesttrayID";
            this.tbTesttrayID.Size = new System.Drawing.Size(110, 20);
            this.tbTesttrayID.TabIndex = 12;
            this.tbTesttrayID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(17, 23);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(66, 13);
            this.label15.TabIndex = 11;
            this.label15.Text = "Test Tray ID";
            // 
            // tbNumFogLamps
            // 
            this.tbNumFogLamps.Location = new System.Drawing.Point(16, 83);
            this.tbNumFogLamps.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbNumFogLamps.Name = "tbNumFogLamps";
            this.tbNumFogLamps.Size = new System.Drawing.Size(110, 20);
            this.tbNumFogLamps.TabIndex = 10;
            this.tbNumFogLamps.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 67);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "# Fog Lamps";
            // 
            // gpStationConfig
            // 
            this.gpStationConfig.Controls.Add(this.tbWorkerSpeed);
            this.gpStationConfig.Controls.Add(this.label14);
            this.gpStationConfig.Controls.Add(this.tbTimeCorrection);
            this.gpStationConfig.Controls.Add(this.label11);
            this.gpStationConfig.Controls.Add(this.tbErrorMargin);
            this.gpStationConfig.Controls.Add(this.label8);
            this.gpStationConfig.Controls.Add(this.tbStartTime);
            this.gpStationConfig.Controls.Add(this.label10);
            this.gpStationConfig.Controls.Add(this.tbStationID);
            this.gpStationConfig.Controls.Add(this.label9);
            this.gpStationConfig.Controls.Add(this.tbWorkerExperience);
            this.gpStationConfig.Controls.Add(this.label7);
            this.gpStationConfig.Location = new System.Drawing.Point(9, 117);
            this.gpStationConfig.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gpStationConfig.Name = "gpStationConfig";
            this.gpStationConfig.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gpStationConfig.Size = new System.Drawing.Size(422, 119);
            this.gpStationConfig.TabIndex = 3;
            this.gpStationConfig.TabStop = false;
            this.gpStationConfig.Text = "Station Config";
            // 
            // tbWorkerSpeed
            // 
            this.tbWorkerSpeed.Location = new System.Drawing.Point(296, 39);
            this.tbWorkerSpeed.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbWorkerSpeed.Name = "tbWorkerSpeed";
            this.tbWorkerSpeed.Size = new System.Drawing.Size(108, 20);
            this.tbWorkerSpeed.TabIndex = 20;
            this.tbWorkerSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(296, 23);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "Worker Speed (sec)";
            // 
            // tbTimeCorrection
            // 
            this.tbTimeCorrection.Location = new System.Drawing.Point(296, 83);
            this.tbTimeCorrection.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbTimeCorrection.Name = "tbTimeCorrection";
            this.tbTimeCorrection.Size = new System.Drawing.Size(108, 20);
            this.tbTimeCorrection.TabIndex = 18;
            this.tbTimeCorrection.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(296, 67);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "Time Correction";
            // 
            // tbErrorMargin
            // 
            this.tbErrorMargin.Location = new System.Drawing.Point(155, 83);
            this.tbErrorMargin.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbErrorMargin.Name = "tbErrorMargin";
            this.tbErrorMargin.Size = new System.Drawing.Size(108, 20);
            this.tbErrorMargin.TabIndex = 16;
            this.tbErrorMargin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(156, 67);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Error Margin";
            // 
            // tbStartTime
            // 
            this.tbStartTime.Location = new System.Drawing.Point(18, 83);
            this.tbStartTime.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbStartTime.Name = "tbStartTime";
            this.tbStartTime.Size = new System.Drawing.Size(108, 20);
            this.tbStartTime.TabIndex = 14;
            this.tbStartTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(19, 67);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Start Time";
            // 
            // tbStationID
            // 
            this.tbStationID.Location = new System.Drawing.Point(18, 39);
            this.tbStationID.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbStationID.Name = "tbStationID";
            this.tbStationID.Size = new System.Drawing.Size(108, 20);
            this.tbStationID.TabIndex = 12;
            this.tbStationID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 23);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Station ID";
            // 
            // tbWorkerExperience
            // 
            this.tbWorkerExperience.Location = new System.Drawing.Point(155, 39);
            this.tbWorkerExperience.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbWorkerExperience.Name = "tbWorkerExperience";
            this.tbWorkerExperience.Size = new System.Drawing.Size(108, 20);
            this.tbWorkerExperience.TabIndex = 10;
            this.tbWorkerExperience.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(156, 23);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Worker Experience";
            // 
            // StationTimer
            // 
            this.StationTimer.Interval = 1000;
            this.StationTimer.Tick += new System.EventHandler(this.StationTimer_Tick);
            // 
            // Station
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 246);
            this.Controls.Add(this.gpStationConfig);
            this.Controls.Add(this.gpTestTray);
            this.Controls.Add(this.gpBins);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Station";
            this.Text = "Station";
            this.gpBins.ResumeLayout(false);
            this.gpBins.PerformLayout();
            this.gpTestTray.ResumeLayout(false);
            this.gpTestTray.PerformLayout();
            this.gpStationConfig.ResumeLayout(false);
            this.gpStationConfig.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpBins;
        private System.Windows.Forms.TextBox tbBezel;
        private System.Windows.Forms.TextBox tbBulb;
        private System.Windows.Forms.TextBox tbLens;
        private System.Windows.Forms.TextBox tbHousing;
        private System.Windows.Forms.TextBox tbReflector;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbHarness;
        private System.Windows.Forms.Panel plBezelColor;
        private System.Windows.Forms.Panel plBulbColor;
        private System.Windows.Forms.Panel plLensColor;
        private System.Windows.Forms.Panel plHousingColor;
        private System.Windows.Forms.Panel plReflectorColor;
        private System.Windows.Forms.Panel plHarnessColor;
        private System.Windows.Forms.GroupBox gpTestTray;
        private System.Windows.Forms.TextBox tbNumFogLamps;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox gpStationConfig;
        private System.Windows.Forms.TextBox tbTimeCorrection;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbErrorMargin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbStartTime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbStationID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbWorkerExperience;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbWorkerSpeed;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbTesttrayID;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Timer StationTimer;
    }
}