﻿namespace FLStation
{
    partial class StationStarter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StationStarter));
            this.lblChooseOperator = new System.Windows.Forms.Label();
            this.cbxWCategories = new System.Windows.Forms.ComboBox();
            this.btnStartStation = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblChooseOperator
            // 
            this.lblChooseOperator.AutoSize = true;
            this.lblChooseOperator.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.142858F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChooseOperator.Location = new System.Drawing.Point(7, 7);
            this.lblChooseOperator.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblChooseOperator.Name = "lblChooseOperator";
            this.lblChooseOperator.Size = new System.Drawing.Size(346, 13);
            this.lblChooseOperator.TabIndex = 0;
            this.lblChooseOperator.Text = "Please, select the type of operator you want on the Station:";
            // 
            // cbxWCategories
            // 
            this.cbxWCategories.FormattingEnabled = true;
            this.cbxWCategories.Location = new System.Drawing.Point(8, 37);
            this.cbxWCategories.Margin = new System.Windows.Forms.Padding(2);
            this.cbxWCategories.Name = "cbxWCategories";
            this.cbxWCategories.Size = new System.Drawing.Size(414, 21);
            this.cbxWCategories.TabIndex = 1;
            // 
            // btnStartStation
            // 
            this.btnStartStation.Location = new System.Drawing.Point(233, 67);
            this.btnStartStation.Margin = new System.Windows.Forms.Padding(2);
            this.btnStartStation.Name = "btnStartStation";
            this.btnStartStation.Size = new System.Drawing.Size(188, 20);
            this.btnStartStation.TabIndex = 2;
            this.btnStartStation.Text = "Start Station";
            this.btnStartStation.UseVisualStyleBackColor = true;
            this.btnStartStation.Click += new System.EventHandler(this.btnStartStation_Click);
            // 
            // StationStarter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 104);
            this.Controls.Add(this.btnStartStation);
            this.Controls.Add(this.cbxWCategories);
            this.Controls.Add(this.lblChooseOperator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "StationStarter";
            this.Text = "Station Starter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblChooseOperator;
        private System.Windows.Forms.ComboBox cbxWCategories;
        private System.Windows.Forms.Button btnStartStation;
    }
}

