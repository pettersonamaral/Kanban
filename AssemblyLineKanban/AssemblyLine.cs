﻿/*****************************************************************************************************
* FILE : 		  AssemblyLine.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-Apr-22


* DESCRIPTION/Requirements: 
    Creates the Runner Class as Singleton and its methods
  
    
* Refence/sources:
*****************************************************************************************************/
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using SQL_FinalProject;


namespace AssemblyLineKanban
{
    public sealed class AssemblyLine
    {
        #region Public Constants 
        public const String LabelStarted = "AssemblyLine started.";
        public const String LabelStopped = "AssemblyLine stopped.";
        #endregion


        #region Proprieties
        public Boolean IsAssemblyLineStarted    { get; set; }                                   // Set / Get runner status (Started = True || Stopped = False)
        public Single TimeScale                 { get; set; }                                   // Get time scale
        public Int32 AssemblyLineTimer          { get; set; }                                   // Get time scale

        #endregion


        #region attribuit
        private static volatile AssemblyLine instance;                                          // Implementation Strategy - Multithreaded Singleton
        private static object syncRoot = new Object();                                          // Implementation Strategy - Multithreaded Singleton
        private Database myDatabase = Database.Instance;                                        // Load/Get Database Class instance
        static private SqlCommand cmd = new SqlCommand();

        #endregion


        #region Construtor
        /// <summary>
        /// Default constructor
        /// </summary>
        private AssemblyLine()
        {
            IsAssemblyLineStarted = false;
        }
        #endregion


        #region Support Methods
        // --- Starts Singleton Pattern
        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        public static AssemblyLine Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new AssemblyLine();
                    }
                }
                return instance;
            }
        }
        // --- ends Singleton Pattern


        /// <summary>
        /// It starts the AssemblyLine
        /// </summary>
        /// <returns></returns>
        public bool Start()
        {
            bool setReturMethod = true;

            if (!GetConfigurationValues()) { setReturMethod = false; }                     // Load all Attributes values
            else { IsAssemblyLineStarted = true; }                                         // If GetConfiguration doesn't fail returns true.

            return setReturMethod;
        }


        /// <summary>
        /// It stops the AssemblyLine
        /// </summary>
        /// <returns></returns>
        public bool Stop()
        {
            bool setReturMethod = true;


            IsAssemblyLineStarted = false;
            return setReturMethod;
        }

        #endregion


        #region Graphis

        /// <summary>
        /// Return a result of a Storage procedure
        /// </summary>
        /// <returns></returns>
        public DataTable ProductionBySation()
        {
            String query = "sp_ProductionBySation";
            DataTable sp_ProductionBySation = new DataTable();

            try
            {
                myDatabase.Conn.Open();                                   // Open connection

                using (SqlCommand mySqlCommand = new SqlCommand(query, myDatabase.Conn))
                {
                    mySqlCommand.CommandType = CommandType.StoredProcedure;
                    mySqlCommand.ExecuteNonQuery();
                    SqlDataAdapter da = new SqlDataAdapter(mySqlCommand);
                    da.Fill(sp_ProductionBySation);
                }

                myDatabase.Conn.Close();                                  // Close connection
            }
            catch (Exception ex)
            {
                myDatabase.Conn.Close();
                IsAssemblyLineStarted = false;
                MessageBox.Show(ex.Message.ToString(), "Failure to run AssemblyLine!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return sp_ProductionBySation;
        }


        /// <summary>
        /// Return a result of a Storage procedure
        /// </summary>
        /// <returns></returns>
        public DataTable ProductionByProfile()
        {
            String query = "sp_ProductionByProfile";
            DataTable sp_ProductionByProfile = new DataTable();

            try
            {
                myDatabase.Conn.Open();                                   // Open connection

                using (SqlCommand mySqlCommand = new SqlCommand(query, myDatabase.Conn))
                {
                    mySqlCommand.CommandType = CommandType.StoredProcedure;
                    mySqlCommand.ExecuteNonQuery();
                    SqlDataAdapter da = new SqlDataAdapter(mySqlCommand);
                    da.Fill(sp_ProductionByProfile);
                }

                myDatabase.Conn.Close();                                  // Close connection
            }
            catch (Exception ex)
            {
                myDatabase.Conn.Close();
                IsAssemblyLineStarted = false;
                MessageBox.Show(ex.Message.ToString(), "Failure to run AssemblyLine!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return sp_ProductionByProfile;
        }


        /// <summary>
        /// Return a result of a Storage procedure
        /// </summary>
        /// <returns></returns>
        public DataTable PassFail()
        {
            String query = "sp_PassFail";
            DataTable passFail = new DataTable();

            try
            {
                myDatabase.Conn.Open();                                   // Open connection

                using (SqlCommand mySqlCommand = new SqlCommand(query, myDatabase.Conn))
                {
                    mySqlCommand.CommandType = CommandType.StoredProcedure;
                    mySqlCommand.ExecuteNonQuery();
                    SqlDataAdapter da = new SqlDataAdapter(mySqlCommand);
                    da.Fill(passFail);
                }

                myDatabase.Conn.Close();                                  // Close connection
            }
            catch (Exception ex)
            {
                myDatabase.Conn.Close();
                IsAssemblyLineStarted = false;
                MessageBox.Show(ex.Message.ToString(), "Failure to run AssemblyLine!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return passFail;
        }

        #endregion


        #region Get values from cfg table

        /// <summary>
        /// It stops the AssemblyLine
        /// </summary>
        /// <returns></returns>
        private bool GetConfigurationValues()
        {
            bool setReturMethod = true;

            if (!GetTimeScale())    { setReturMethod = false; }
            if (!GetAssemblyLineTime())   { setReturMethod = false; }

            return setReturMethod;
        }


        /// <summary>
        /// Get timescale value from the database
        /// </summary>
        private bool GetTimeScale()
        {
            bool setReturMethod = true;

            String query = Helper.GetSelectTimeScale();
            SqlDataReader myDataReader = null;

            try
            {
                myDatabase.Conn.Open();                                   // Open connection
                cmd.Connection = myDatabase.Conn;                         // Load command.Connection Attribute with value from connection

                cmd.CommandText = query;                                  // Load command.CommandText Attribute with value from query

                myDataReader = cmd.ExecuteReader();

                while (myDataReader.Read())
                { 
                    TimeScale = Convert.ToSingle(myDataReader["value"]);
                }

                myDatabase.Conn.Close();                                  // Close connection
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                myDatabase.Conn.Close();
                setReturMethod = false;
                MessageBox.Show(ex.Message.ToString(), "Failure to load TimeScale value!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return setReturMethod;
        }


        /// <summary>
        /// Get AssemblyLine_time value from the database
        /// </summary>
        private bool GetAssemblyLineTime()
        {
            bool setReturMethod = true;

            String query = Helper.GetAssemblyLineTimer();
            SqlDataReader myDataReader = null;

            try
            {
                myDatabase.Conn.Open();                                   // Open connection
                cmd.Connection = myDatabase.Conn;                         // Load command.Connection Attribute with value from connection

                cmd.CommandText = query;                                  // Load command.CommandText Attribute with value from query

                myDataReader = cmd.ExecuteReader();

                while (myDataReader.Read())
                {
                    AssemblyLineTimer = Convert.ToInt32(myDataReader["value"]);
                }

                myDatabase.Conn.Close();                                  // Close connection
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                myDatabase.Conn.Close();
                setReturMethod = false;
                MessageBox.Show(ex.Message.ToString(), "Failure to load AssemblyLine time value!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return setReturMethod;
        }
    }
    #endregion
}

