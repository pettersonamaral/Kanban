﻿namespace AssemblyLineKanban
{
    partial class frmAssemblyLine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAssemblyLine));
            this.grpBoxSource = new System.Windows.Forms.GroupBox();
            this.picConnected = new System.Windows.Forms.PictureBox();
            this.grpBoxSourceSummary = new System.Windows.Forms.GroupBox();
            this.lblSourceDbNameStatus = new System.Windows.Forms.Label();
            this.lblSourceConnectionStatus = new System.Windows.Forms.Label();
            this.btnSourceConnection = new System.Windows.Forms.Button();
            this.assemblyLineTimer = new System.Windows.Forms.Timer(this.components);
            this.grpBoxControls = new System.Windows.Forms.GroupBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblControlTimer = new System.Windows.Forms.Label();
            this.lblControlSatus = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.lblSpace = new System.Windows.Forms.Label();
            this.chartByStation = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartByProfife = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartByPass = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.grpBoxSource.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picConnected)).BeginInit();
            this.grpBoxSourceSummary.SuspendLayout();
            this.grpBoxControls.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartByStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartByProfife)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartByPass)).BeginInit();
            this.SuspendLayout();
            // 
            // grpBoxSource
            // 
            this.grpBoxSource.Controls.Add(this.picConnected);
            this.grpBoxSource.Controls.Add(this.grpBoxSourceSummary);
            this.grpBoxSource.Controls.Add(this.btnSourceConnection);
            this.grpBoxSource.Location = new System.Drawing.Point(12, 12);
            this.grpBoxSource.Name = "grpBoxSource";
            this.grpBoxSource.Size = new System.Drawing.Size(271, 124);
            this.grpBoxSource.TabIndex = 8;
            this.grpBoxSource.TabStop = false;
            this.grpBoxSource.Text = "Control Commands";
            // 
            // picConnected
            // 
            this.picConnected.Enabled = false;
            this.picConnected.Image = global::AssemblyLineKanban.Properties.Resources.connected;
            this.picConnected.InitialImage = global::AssemblyLineKanban.Properties.Resources.connected;
            this.picConnected.Location = new System.Drawing.Point(105, 20);
            this.picConnected.Name = "picConnected";
            this.picConnected.Size = new System.Drawing.Size(30, 27);
            this.picConnected.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picConnected.TabIndex = 3;
            this.picConnected.TabStop = false;
            this.picConnected.Visible = false;
            // 
            // grpBoxSourceSummary
            // 
            this.grpBoxSourceSummary.Controls.Add(this.lblSourceDbNameStatus);
            this.grpBoxSourceSummary.Controls.Add(this.lblSourceConnectionStatus);
            this.grpBoxSourceSummary.Location = new System.Drawing.Point(6, 53);
            this.grpBoxSourceSummary.Name = "grpBoxSourceSummary";
            this.grpBoxSourceSummary.Size = new System.Drawing.Size(259, 64);
            this.grpBoxSourceSummary.TabIndex = 2;
            this.grpBoxSourceSummary.TabStop = false;
            this.grpBoxSourceSummary.Text = "Source connection";
            // 
            // lblSourceDbNameStatus
            // 
            this.lblSourceDbNameStatus.AutoSize = true;
            this.lblSourceDbNameStatus.Location = new System.Drawing.Point(7, 42);
            this.lblSourceDbNameStatus.Name = "lblSourceDbNameStatus";
            this.lblSourceDbNameStatus.Size = new System.Drawing.Size(0, 13);
            this.lblSourceDbNameStatus.TabIndex = 2;
            // 
            // lblSourceConnectionStatus
            // 
            this.lblSourceConnectionStatus.AutoSize = true;
            this.lblSourceConnectionStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSourceConnectionStatus.Location = new System.Drawing.Point(7, 20);
            this.lblSourceConnectionStatus.Name = "lblSourceConnectionStatus";
            this.lblSourceConnectionStatus.Size = new System.Drawing.Size(0, 13);
            this.lblSourceConnectionStatus.TabIndex = 0;
            // 
            // btnSourceConnection
            // 
            this.btnSourceConnection.Location = new System.Drawing.Point(6, 19);
            this.btnSourceConnection.Name = "btnSourceConnection";
            this.btnSourceConnection.Size = new System.Drawing.Size(92, 28);
            this.btnSourceConnection.TabIndex = 0;
            this.btnSourceConnection.Text = "Connection...";
            this.btnSourceConnection.UseVisualStyleBackColor = true;
            this.btnSourceConnection.Click += new System.EventHandler(this.BtnSourceConnection_Click);
            // 
            // assemblyLineTimer
            // 
            this.assemblyLineTimer.Tick += new System.EventHandler(this.assemblyLineTimer_Tick);
            // 
            // grpBoxControls
            // 
            this.grpBoxControls.Controls.Add(this.btnStop);
            this.grpBoxControls.Controls.Add(this.groupBox2);
            this.grpBoxControls.Controls.Add(this.btnStart);
            this.grpBoxControls.Location = new System.Drawing.Point(12, 161);
            this.grpBoxControls.Name = "grpBoxControls";
            this.grpBoxControls.Size = new System.Drawing.Size(271, 124);
            this.grpBoxControls.TabIndex = 9;
            this.grpBoxControls.TabStop = false;
            this.grpBoxControls.Text = "Controls";
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(87, 19);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 28);
            this.btnStop.TabIndex = 3;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.BtnStop_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblControlTimer);
            this.groupBox2.Controls.Add(this.lblControlSatus);
            this.groupBox2.Location = new System.Drawing.Point(6, 53);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(259, 64);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Status";
            // 
            // lblControlTimer
            // 
            this.lblControlTimer.AutoSize = true;
            this.lblControlTimer.Location = new System.Drawing.Point(7, 42);
            this.lblControlTimer.Name = "lblControlTimer";
            this.lblControlTimer.Size = new System.Drawing.Size(0, 13);
            this.lblControlTimer.TabIndex = 2;
            // 
            // lblControlSatus
            // 
            this.lblControlSatus.AutoSize = true;
            this.lblControlSatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblControlSatus.Location = new System.Drawing.Point(7, 20);
            this.lblControlSatus.Name = "lblControlSatus";
            this.lblControlSatus.Size = new System.Drawing.Size(0, 13);
            this.lblControlSatus.TabIndex = 0;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(6, 19);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 28);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // lblSpace
            // 
            this.lblSpace.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSpace.Location = new System.Drawing.Point(13, 149);
            this.lblSpace.Name = "lblSpace";
            this.lblSpace.Size = new System.Drawing.Size(270, 2);
            this.lblSpace.TabIndex = 10;
            // 
            // chartByStation
            // 
            chartArea1.Name = "ChartArea1";
            this.chartByStation.ChartAreas.Add(chartArea1);
            this.chartByStation.Location = new System.Drawing.Point(289, 292);
            this.chartByStation.Name = "chartByStation";
            series1.ChartArea = "ChartArea1";
            series1.Name = "Series1";
            this.chartByStation.Series.Add(series1);
            this.chartByStation.Size = new System.Drawing.Size(479, 299);
            this.chartByStation.TabIndex = 11;
            this.chartByStation.Text = "chart1";
            // 
            // chartByProfife
            // 
            chartArea2.Name = "ChartArea1";
            this.chartByProfife.ChartAreas.Add(chartArea2);
            this.chartByProfife.Location = new System.Drawing.Point(289, 12);
            this.chartByProfife.Name = "chartByProfife";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Bar;
            series2.IsVisibleInLegend = false;
            series2.Name = "Series1";
            this.chartByProfife.Series.Add(series2);
            this.chartByProfife.Size = new System.Drawing.Size(479, 273);
            this.chartByProfife.TabIndex = 12;
            this.chartByProfife.Text = "chart2";
            // 
            // chartByPass
            // 
            chartArea3.Name = "ChartArea1";
            this.chartByPass.ChartAreas.Add(chartArea3);
            this.chartByPass.Location = new System.Drawing.Point(12, 292);
            this.chartByPass.Name = "chartByPass";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series3.IsVisibleInLegend = false;
            series3.Name = "Series1";
            this.chartByPass.Series.Add(series3);
            this.chartByPass.Size = new System.Drawing.Size(271, 299);
            this.chartByPass.TabIndex = 13;
            this.chartByPass.Text = "chart2";
            // 
            // frmAssemblyLine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 603);
            this.Controls.Add(this.chartByPass);
            this.Controls.Add(this.chartByProfife);
            this.Controls.Add(this.chartByStation);
            this.Controls.Add(this.grpBoxSource);
            this.Controls.Add(this.grpBoxControls);
            this.Controls.Add(this.lblSpace);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAssemblyLine";
            this.Text = "Assembly line Kanban";
            this.grpBoxSource.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picConnected)).EndInit();
            this.grpBoxSourceSummary.ResumeLayout(false);
            this.grpBoxSourceSummary.PerformLayout();
            this.grpBoxControls.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartByStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartByProfife)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartByPass)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpBoxSource;
        private System.Windows.Forms.PictureBox picConnected;
        private System.Windows.Forms.GroupBox grpBoxSourceSummary;
        private System.Windows.Forms.Label lblSourceDbNameStatus;
        private System.Windows.Forms.Label lblSourceConnectionStatus;
        private System.Windows.Forms.Button btnSourceConnection;
        private System.Windows.Forms.Timer assemblyLineTimer;
        private System.Windows.Forms.GroupBox grpBoxControls;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblControlTimer;
        private System.Windows.Forms.Label lblControlSatus;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label lblSpace;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartByStation;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartByProfife;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartByPass;
    }
}

