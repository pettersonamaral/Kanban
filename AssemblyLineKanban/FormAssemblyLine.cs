﻿/*****************************************************************************************************
* FILE : 		  FormAssemblyLine.cs
* PROJECT : 	  Kanban 
* PROGRAMMER : 	  PAULO CASADO and PETTERSON AMARAL
* FIRST VERSION:  2018-APR-22


* DESCRIPTION/Requirements: 
1. To develop a SQL-based simulation solution
2. To create and use triggers, stored procedures, stored functions and views to model 
   business/production line workflow

   The customer builds a fog lamp assembly for automobiles. The assembly is made up of a harness, a 
   reflector, a housing, a lens, a bulb and a bezel. Each of these parts is in its bin, and an 
   employee takes one of each of these parts and puts it together. Once completed, the fog lamp is 
   placed on a tray with separators. When full, the tray is sent to the testing department (e.g. 
   these are the ‘test trays’ mentioned below).

* Refence/sources:

*****************************************************************************************************/
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using SQL_FinalProject;


namespace AssemblyLineKanban
{
    public partial class frmAssemblyLine : Form
    {
        private Database myDatabase = Database.Instance;                //Load/Get Database Class instance
        private AssemblyLine myAssemblyLine = AssemblyLine.Instance;    //Load/Get AssemblyLine Class instance
        private const Int32 ticksPerSeconds = 1000;                     //A timer multiplier

        public frmAssemblyLine()
        {
            InitializeComponent();

            if (!myDatabase.ConnectionStatus)                           // If myDatabase is NOT connected
            {
                ItIsDisconnected();
            }
        }

        #region Support Button Methods
        /// <summary>
        /// Button Connection. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSourceConnection_Click(object sender, EventArgs e)
        {
            try
            {
                if (myDatabase.ConnectionStatus)                            // If myDatabase is already connected
                {
                    if (myDatabase.Disconnect())                            // If set up a new connection is true
                    {
                        ItIsDisconnected();
                    }
                }
                else                                                        // If myDatabase is NOT connected
                {
                    if (myDatabase.Connect())                               // If set up a new connection is return true
                    {
                        ItIsConnected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Exception!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        /// <summary>
        /// Controls the Start button action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnStart_Click(object sender, EventArgs e)
        {
            StartAssemblyLine();
        }


        /// <summary>
        /// Controls the Stop button action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnStop_Click(object sender, EventArgs e)
        {
            StopAssemblyLine();
        }
        #endregion


        #region Support Methods

        /// <summary>
        /// Support method to Start AssemblyLine
        /// </summary>
        private void ToggleAssemblyLineButtons()
        {
            if (myAssemblyLine.IsAssemblyLineStarted)
            {
                btnStart.Enabled = false;
                btnStop.Enabled = true;
            }
            else
            {
                btnStart.Enabled = true;
                btnStop.Enabled = false;
            }
        }


        /// <summary>
        /// Support method to Connected status true
        /// </summary>
        private void ItIsConnected()
        {
            lblSourceConnectionStatus.Text = Database.CONNECTEDSTATUS;
            lblSourceDbNameStatus.Text = Database.DBNAME + myDatabase.DbName.ToString();
            picConnected.Visible = true;
            myDatabase.ConnectionStatus = true;
            btnStart.Enabled = true;
        }


        /// <summary>
        /// Support method to Connected status false
        /// </summary>
        private void ItIsDisconnected()
        {
            StopAssemblyLine();
            lblSourceConnectionStatus.Text = Database.DISCONNECTEDSTATUS;
            lblSourceDbNameStatus.Text = "";
            picConnected.Visible = false;
            myDatabase.ConnectionStatus = false;
            btnStart.Enabled = false;
            btnStop.Enabled = false;
        }


        /// <summary>
        /// Support method to Stop AssemblyLine
        /// </summary>
        private void StopAssemblyLine()
        {
            assemblyLineTimer.Stop();

            if (myAssemblyLine.Stop())
            {
                lblControlSatus.Text = AssemblyLine.LabelStopped;
                lblControlTimer.Text = "";
                ToggleAssemblyLineButtons();
            }
        }


        /// <summary>
        /// Support method to Start AssemblyLine
        /// </summary>
        private void StartAssemblyLine()
        {
            if (myAssemblyLine.Start())
            {
                ToggleAssemblyLineButtons();
                lblControlSatus.Text = AssemblyLine.LabelStarted;
                //assemblyLineTimer.Interval = (Int32)((ticksPerSeconds * myAssemblyLine.AssemblyLineTimer * 60) / myAssemblyLine.TimeScale);
                assemblyLineTimer.Interval = (Int32)(3000);
                assemblyLineTimer.Start();
            }
        }


        /// <summary>
        /// Tick timer function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void assemblyLineTimer_Tick(object sender, EventArgs e)
        {
            if (myAssemblyLine.IsAssemblyLineStarted)
            {
                LoadGraphs();
            }
            else
            {
                StopAssemblyLine();
            }
        }
        #endregion


        #region Support Graphs Methods

        /// <summary>
        /// Support method to Load all graphs
        /// </summary>
        private void LoadGraphs()
        {
            ProductionBySationGraph();
            ProductionByProfileGraphs();
            PassFailGraphs();

        }


        /// <summary>
        /// Support method to start the graphic ProductionBySation
        /// </summary>
        private void ProductionBySationGraph()
        {
            Title myTitle = null;
            String title = "Production by station";

            try
            {
                chartByStation.DataSource = myAssemblyLine.ProductionBySation();

                chartByStation.Titles.Clear();
                myTitle = new Title(title, Docking.Top, new System.Drawing.Font("Calibri", 14, FontStyle.Bold), Color.Black);
                chartByStation.Titles.Add(myTitle);
                chartByStation.Series[0].XValueMember = "StationID";
                chartByStation.Series[0].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
                chartByStation.ChartAreas[0].AxisX.Title = "StationID";

                chartByStation.Series[0].YValueMembers = "Totalqty";
                chartByStation.Series[0].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
                chartByStation.ChartAreas[0].AxisY.Title = "Total Qty";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Failure to run load Production by station graph!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        /// <summary>
        /// Support method to start the graphic ProductionByProfile
        /// </summary>
        private void ProductionByProfileGraphs()
        {
            Title myTitle = null;
            String title = "Production by profile";

            try
            {
                chartByProfife.DataSource = myAssemblyLine.ProductionByProfile();

                chartByProfife.Titles.Clear();
                myTitle = new Title(title, Docking.Top, new System.Drawing.Font("Calibri", 14, FontStyle.Bold), Color.Black);
                chartByProfife.Titles.Add(myTitle);
                chartByProfife.Series[0].XValueMember = "WorkerExperience";
                chartByProfife.Series[0].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;

                chartByProfife.Series[0].YValueMembers = "TotProfile";
                chartByProfife.Series[0].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
                chartByProfife.ChartAreas[0].AxisY.Title = "Total Qty";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Failure to run load Production by profile graph!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        /// <summary>
        /// Support method to start the graphic PassFail
        /// </summary>
        private void PassFailGraphs()
        {
            Title myTitle = null;
            String title = "Pass-fail proportion";

            try
            {
                chartByPass.DataSource = myAssemblyLine.PassFail();

                chartByPass.Titles.Clear();
                myTitle = new Title(title, Docking.Top, new Font("Calibri", 14, FontStyle.Bold), Color.Black);
                chartByPass.Titles.Add(myTitle);
               
                chartByPass.Series[0].XValueMember = "TestPass";
                chartByPass.Series[0].XValueType = ChartValueType.Int32;

                chartByPass.Series[0].YValueMembers = "CountPassFail";
                chartByPass.Series[0].YValueType = ChartValueType.Int32;
                chartByPass.ChartAreas[0].AxisY.Title = "Total Qty";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Failure to run load Pass-fail proportion graph!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #endregion
    }
}
